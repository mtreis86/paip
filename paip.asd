#|
  This file is a part of paip project.
|#

(defsystem "paip"
  :version "0.0.1"
  :author "Michael Reis <michael@mtreis86.com>"
  :license "public domain"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "constants")
                 (:file "debugging")
                 (:file "memo")
                 (:file "utils")
                 (:file "eliza" :depends-on ("debugging" "utils"))
                 (:file "pat-match" :depends-on ("constants" "debugging" "utils"))
                 (:file "student")
                 (:file "grammars")
                 (:file "simplify-slow" :depends-on ("pat-match" "student"))
                 (:file "simplify-fast" :depends-on ("simplify-slow" "memo"))
                 (:file "unify" :depends-on ("pat-match"))
                 (:file "prolog-i" :depends-on ("unify"))
                 (:file "prolog-c" :depends-on ("prolog-i"))
                 (:file "search" :depends-on ("constants" "debugging" "utils"))
                 (:file "search-clos" :depends-on ("search"))

                 ;; (:file "ch1")
                 ;; (:file "ch2")
                 ;; (:file "ch3")
                 ;; (:file "ch4" :depends-on ("debugging"
                 ;;                           "utils"))
                 ;; (:file "ch5" :depends-on ("constants"
                 ;;                           "debuggins"))
                 ;; (:file "ch6" :depends-on ("constants"
                 ;;                           "debugging"
                 ;;                           "utils"))
                 ;; (:file "ch7" :depends-on ("constants"
                 ;;                           "pat-match"
                 ;;                           "utils"))
                 ;; (:file "ch8" :depends-on ("pat-match" "student")))))
                 ;; (:file "ch9" :depends-on ("grammars" "simplify-slow"))
                 ;; (:file "ch10" :depends-on ("constants"))
                 ;; (:file "ch11" :depends-on ("pat-match"))
                 ;; (:file "ch12" :depends-on ("prolog-i" "simplify-fast"))
                 ;; (:file "ch13" :depends-on ("utils" "debugging" "search"))
                 ;; (:file "ch14" :depends-on ("prolog-c" "utils"))
                 (:file "ch15" :depends-on ("pat-match")))))
  :description "examples and exercises from the book paradigms of artifical intelligence programming"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "paip-test"))))
