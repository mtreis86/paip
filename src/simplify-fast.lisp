(in-package :paip)

(defvar *test-data*
  (mapcar #'infix->prefix
          '((d (a * x ^ 2 + b * x + c) / d x)
            (d ((a * x ^ 2 + b * x + c * x + d) / x ^ 5) / d x)
            ((sin (x + x)) * (sin (2 * x)) + (cos (d (x ^ 2) / d x )) ^ 1)
            (d (3 * x + (cos x) / x) / d x))))

(defvar *answers* (mapcar #'simplify *test-data*))

(defun test-it (&optional (with-profiling t))
  "Time a test run and make sure the answers are correct."
  (let ((answers
          (if with-profiling
              (with-profiling (simplify simplify-exp pat-match
                                        match-variable variable-p)
                (mapcar #'simplify *test-data*))
              (time (mapcar #'simplify *test-data*)))))
    (mapc #'assert-equal answers *answers*)
    t))

(defun assert-equal (x y)
  "If x is not equal to y, complain."
  (assert (equal x y) (x y)
          "Expected ~a to be equal to ~a" x y))

(defun simplify-exp (exp)
  "Simplify using a rule, or by doing arithmetic, or by using the simp function provided by this operator. This version indexes simplification rules under the operator."
  (cond ((simplify-by-fn exp))
        ((rule-based-translator exp (rules-for (exp-op exp))
                                :rule-if #'exp-lhs :rule-then #'exp-rhs
                                :action #'(lambda (bindings response)
                                            (simplify
                                             (sublis bindings response)))))
        ((evaluable exp) (eval exp))
        (t exp)))

(defvar *rules-for* (make-hash-table :test #'eq))

(defun main-op (rule) (exp-op (exp-lhs rule)))

(defun index-rules (rules)
  "Index all the rules under the main op."
  (clrhash *rules-for*)
  (dolist (rule rules)
    ;;nconc instead of push to preserve the order of rules.
    (setf (gethash (main-op rule) *rules-for*)
          (nconc (gethash (main-op rule) *rules-for*)
                 (list rule)))))

(defun rules-for (op) (gethash op *rules-for*))

(index-rules *simplification-rules*)

(memoize 'simplify :test #'equal)

(defvar *bindings* nil "A list of bindings used by the compiler.")

(defun compile-rule (rule)
  "Compile a single rule."
  (let ((*bindings* nil))
    `(lambda (x)
       ,(compile-exp 'x (exp-lhs rule)
                     (delay (build-exp (exp-rhs rule) *bindings*))))))

(defun compile-exp (var pattern consequent)
  "Compile code that tests the expression, and does consequent if it matches. Assume the bindings in *bindings*."
  (cond
    ;;Test a previously bound variable:
    ((get-binding pattern *bindings*)
     `(if (equal ,var ,(lookup pattern *bindings*))
          ,(force consequent)))
    ;;Add a new binding; do type checking if needed:
    ((variable-p pattern)
     (push (cons pattern var) *bindings*)
     (force consequent))
    ;;Match a literal atom:
    ((atom pattern)
     `(if (eql ,var ',pattern)
          ,(force consequent)))
    ;;Rules:
    ((starts-with pattern '?is)
     (push (cons (second pattern) var) *bindings*)
     `(if (,(third pattern) ,var)
          ,(force consequent)))
    ;;Add code here to compile for other rules:
    ;;Check the operator and arguements
    (t `(if (op? ,var ',(exp-op pattern))
            ,(compile-args var pattern consequent)))))

(defun compile-args (var pattern consequent)
  "Compile code that checks the arg or args, and does consequent if the arg(s) match."
  ;;Make up variable names for the args:
  (let ((L (symbol-concat var 'L))
        (R (symbol-concat var 'R)))
    (if (exp-rhs pattern)
        ;;Two arg case:
        `(let ((,L (exp-lhs ,var))
               (,R (exp-rhs ,var)))
           ,(compile-exp L (exp-lhs pattern)
                         (delay
                          (compile-exp R (exp-rhs pattern)
                                       consequent))))
        ;;One arg case:
        `(let ((,L (exp-lhs ,var)))
           ,(compile-exp L (exp-lhs pattern) consequent)))))

(defun build-exp (exp bindings)
  "Compile code that will build the exp, given the bindings."
  (cond ((assoc exp bindings) (rest (assoc exp bindings)))
        ((variable-p exp)
         (error "Variable ~a occured on the right-hand
                 side but not left." exp))
        ((atom exp) `',exp)
        (t (let ((new-exp (mapcar #'(lambda (x)
                                      (build-exp x bindings))
                                  exp)))
             `(simplify-exp (list .,new-exp))))))

(defun op? (exp op)
  "Does the exp have the given op as its operator?"
  (and (exp-p exp) (eq (exp-op exp) op)))

(defun symbol-concat (&rest args)
  "Concatenate symbols or strings to form an interned symbol."
  (intern (format nil "~{~a~}" args)))

(defun new-symbol (&rest args)
  "Concatenate symbol or strings to form an uninterned symbol."
  (make-symbol (format nil "~{~a~}" args)))


(defun compile-rule-set (op)
  "Compile all rules indexed under a given main op, and make them into the simp-fn for that op."
  (set-simp-fn op
               (compile nil
                        `(lambda (x)
                           ,(reduce #'combine-rules
                                      (mapcar #'compile-indexed-rule
                                                (rules-for op)))))))

(defun compile-indexed-rule (rule)
  "Compile one rule into lambda-less code. Assume indexing of main op."
  (let ((*bindings* nil))
    (compile-args
     'x (exp-lhs rule)
      (delay (build-exp (exp-rhs rule) *bindings*)))))

(defun combine-rules (a b)
  "Combine the code for two rules into one, maintaining order."
  ;;In the default case, generate the code (or a b),
  ;;but try to be clever and share common code,
  ;;assuming no side effects.
  (cond ((and (listp a) (listp b)
              (= (length a) (length b) 3)
              (equal (first a) (first b))
              (equal (second a) (second b)))
         ;;a=(f x y), b=(f x z) -> (f x (combine-rules (y z))),
         ;;can only apply if f=IF or f=LET
         (list (first a) (second a)
               (combine-rules (third a) (third b))))
        ((matching-ifs a b)
         `(if ,(second a)
              ,(combine-rules (third a) (third b))
              ,(combine-rules (fourth a) (fourth b))))
         ;;a=(or ... (if p y)), b=(if p z) ->
         ;;    (or ... (if p (combine-rules y z)))
         ;;  else a=(r ...) b -> (or ... b)
        ((starts-with a 'or)
         (if (matching-ifs (last1 a) b)
             (append (butlast a)
                     (list (combine-rules (last1 a) b)))
             (append a (list b))))
        ;;a, b -> (or a b)
        (t `(or ,a ,b))))

(defun matching-ifs (a b)
  "Are a and b if statements with the same predicate?"
  (and (starts-with a 'if) (starts-with b 'if)
       (equal (second a) (second b))))

(defun last1 (list)
  "Return the last element of a list but not the cons cell."
  (first (last list)))

(defun compile-all-rules-indexed (rules)
  "Compile a separate fn for each operator, and store it as the simp-fn of that operator."
  (index-rules rules)
  (let ((all-ops (delete-duplicates (mapcar #'main-op rules))))
    (mapc #'compile-rule-set all-ops)))

(compile-all-rules-indexed *simplification-rules*)
