(defpackage paip
  (:use :cl)
  (:export memo
           memoize
           clear-memoize
           defun-memo))
