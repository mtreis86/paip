;;paip examples and exercises chapter 6
;;Michael Reis <michael@mtreis86.com


(defun interactive-interpreter (prompt transformer)
  "Read an expression, transform it, and print the result."
  (loop
    (handler-case
        (progn
          (if (stringp prompt)
              (print prompt)
              (funcall prompt))
          (print (funcall transformer (read))))
      ;;In case of error, do this:
      (error (condition)
        (format t "~&;; Error ~a ignored, back to top level."
                condition)))))

(defun prompt-generator (&optional (num 0) (ctl-string "[~d] "))
  "Return a function that prints prompts like [1] [2] [3], etc."
  #'(lambda () (format t ctl-string (incf num))))

(defun pat-match (pattern input &optional (bindings no-bindings))
  "Match pattern against input in the context of the bindings."
  (cond ((eq bindings fail)
         fail)
        ((variable-p pattern)
         (match-variable pattern input bindings))
        ((eql pattern input)
         bindings)
        ((segment-pattern-p pattern)
         (segment-matcher pattern input bindings))
        ((single-pattern-p pattern)
         (single-matcher pattern input bindings))
        ((and (consp pattern) (consp input))
         (pat-match (rest pattern) (rest input)
                    (pat-match (first pattern) (first input) bindings)))
        (t fail)))

(defun segment-pattern-p (pattern)
  "Is this a segment-matching pattern like ((?* var) . pat)?"
  (and (consp pattern) (consp (first pattern))
       (symbolp (first (first pattern)))
       (segment-match-fn (first (first pattern)))))

(defun single-pattern-p (pattern)
  "Is this a single-matching pattern? Eg (?is x predicate) (?and . patterns) (?or . patterns)."
  (and (consp pattern)
       (single-match-fn (first pattern))))

(defun segment-matcher (pattern input bindings)
  "Call the right function for this kind of segment pattern."
  (funcall (segment-match-fn (first (first pattern)))
           pattern input bindings))

(defun single-matcher (pattern input bindings)
  "Call the right function for this kind of single pattern."
  (funcall (single-match-fn (first pattern))
           (rest pattern) input bindings))

(defun segment-match-fn (x)
  "Get the segment-match function for x."
  (when (symbolp x) (get x 'segment-match)))

(defun single-match-fn (x)
  "Get the single-match function for x."
  (when (symbolp x) (get x 'single-match)))

(setf (get '?is 'single-match) 'match-is)
(setf (get '?or 'single-match) 'match-or)
(setf (get '?and 'single-match) 'match-and)
(setf (get '?not 'single-match) 'match-not)

(setf (get '?* 'segment-match) 'segment-match)
(setf (get '?+ 'segment-match) 'segment-match+)
(setf (get '?? 'segment-match) 'segment-match?)
(setf (get '?if 'segment-match) 'match-if)

(defun match-is (var-and-pred input bindings)
  "Succeed and bind var if the input sitisfies pred, where var-and-pred is the list (var pred)."
  (let* ((var (first var-and-pred))
         (pred (second var-and-pred))
         (new-bindings (pat-match var input bindings)))
    (if (or (eq new-bindings fail)
            (not (funcall pred input)))
        fail
        new-bindings)))

(defun match-and (patterns input bindings)
  "Succeed if all the patterns match the inputs."
  (cond ((eq bindings fail) fail)
        ((null patterns) bindings)
        (t (match-and (rest patterns) input
                      (pat-match (first patterns) input bindings)))))

(defun match-or (patterns input bindings)
  "Succeed if any one of the patterns match the input."
  (if (null patterns)
      fail
      (let ((new-bindings (pat-match (first patterns) input bindings)))
        (if (eq new-bindings fail)
            (match-or (rest patterns) input bindings)))))

(defun match-not (patterns input bindings)
  "Succeed if none of the patterns match the input. This will never bind any variables."
  (if (match-or patterns input bindings)
      fail
      bindings))

(defun segment-match (pattern input bindings &optional (start 0))
  "Match the segment pattern ((?* var) . pat) against input."
  (let ((var (second (first pattern)))
        (pat (rest pattern)))
    (if (null pat)
        (match-variable var input bindings)
        (let ((pos (first-match-pos (first pat) input start)))
          (if (null pos)
              fail
              (let ((b2 (pat-match
                         pat (subseq input pos)
                         (match-variable var (subseq input 0 pos)
                                        bindings))))
                ;;If this match failed, try another longer one
                (if (eq b2 fail)
                    (segment-match pattern input bindings (+ pos 1))
                    b2)))))))

(defun first-match-pos (pat1 input start)
  "Find the first position that pat1 could possibly match input, starting at start position. If pat1 is non-constand, then just return start."
  (cond ((atom pat1) (not (variable-p pat1))
         (position pat1 input :start start :test #'equal))
        ((< start (length input)) start)
        (t nil)))

(defun segment-match+ (pattern input bindings)
  "Match one or more elements of input."
  (segment-match pattern input bindings 1))

(defun segement-match? (pattern input bindings)
  "Match zero or one element of input."
  (let ((var (second (first pattern)))
        (pat (rest pattern)))
    (or (pat-match (cons var pat) input bindings)
        (pat-match pat input bindings))))

(defun match-if (pattern input bindings)
  "Test an arbitrary expression involving variables. The pattern looks like ((?if code) . rest)."
  (and (eval (sublis bindings (second (first pattern))))
       (pat-match (rest pattern) input bindings)))

(defun pat-match-abbrev (symbol expansion)
  "Define symbol as a macro standing for a pat-match pattern."
  (setf (get symbol 'expand-pat-match-abbrev) expansion))

(defun expand-pat-match-abbrev (pat)
  "Expand out all pattern matching abbreviations in pat."
  (cond ((and (symbolp pat) (get pat 'expand-pat-match-abbrev)))
        ((atom pat) pat)
        (t (cons (expand-pat-match-abbrev (first pat))
                 (expand-pat-match-abbrev (rest pat))))))

(defun rule-based-translator (input rules
                              &key (matcher #'pat-match) (rule-if #'first)
                                (rule-then #'rest) (action #'sublis))
  "Find the first rule in rules that matches input, and apply the action to that rule. Allow for alternative matcher, if, then, and action."
  (some
   #'(lambda (rule)
       (let ((result (funcall matcher (funcall rule-if rule)
                              input)))
         (if (not (eq result fail))
             (funcall action result (funcall rule-then rules)))))
   rules))

(defun tree-search (states goal-p successors combiner)
  "Find a state that satisfies goal-p. Start with states, and search according to successors and combiner."
  (dbg :search "~&;; Search: ~a" states)
  (cond ((null states) fail)
        ((funcall goal-p (first states)) (first states))
        (t (tree-search
            (funcall combiner
                     (funcall successors (first states))
                     (rest states))
            goal-p successors combiner))))

(defun depth-first-search (start goal-p successors)
  "Search new states first until goal is reached."
  (tree-search (list start) goal-p successors #'append))

(defun binary-tree (x)
  "Return a function that generates an infinite binary tree."
  (list (* x 2) (+ 1 (* x 2))))

;;(defun is (value) #'(lambda (x) (eql x value)))
(defun is (value &key (key #'identity) (test #'eql))
  "Returns a predicate that tests for a given value."
  #'(lambda (path) (funcall test value (funcall key path))))

(defun breadth-first-search (start goal-p successors)
  "Search old states first until goal is reached."
  (tree-search (list start) goal-p successors #'prepend))

(defun prepend (x y)
  "Prepend y to the start of x."
  (append y x))

(defun finite-binary-tree (n)
  "Return a function that generates a finite binary tree with n nodes."
  #'(lambda (x) (remove-if #'(lambda (child) (> child n))
                           (binary-tree x))))

(defun diff (num)
  "Return the function that finds the difference from num."
  #'(lambda (x) (abs (- x num))))

(defun sorter (cost-fn)
  "Return a combiner function that sorts according to cost-fn."
  #'(lambda (new old)
      (sort (append new old) #'< :key cost-fn)))

(defun best-first-search (start goal-p successors cost-fn)
  "Search lowest cost states first until goal is reached."
  (tree-search (list start) goal-p successors (sorter cost-fn)))

(defun price-is-right (price)
  "Return a function that measures the difference from price, but gives a big penalty for going over price."
  #'(lambda (x) (if (> x price)
                    most-positive-fixnum
                    (- price x))))

(defun beam-search (start goal-p successors cost-fn beam-width)
  "Search for the highest scoring states first until goal is reached, but never consider more than the beam-width states at a time."
  (tree-search (list start) goal-p successors
               #'(lambda (old new)
                   (let ((sorted (funcall (sorter cost-fn) old new)))
                     (if (> beam-width (length sorted))
                         sorted
                         (subseq sorted 0 beam-width))))))

(defstruct (city (:type list)) name long lat)

(defparameter *cities*
  '((Atlanta        84.23 33.45)
    (Boston         71.05 42.21)
    (Chicago        87.37 41.50)
    (Denver        105.00 39.45)
    (Eugene        123.05 44.03)
    (Flagstaff     111.41 35.13)
    (Grand-Jct     108.37 39.05)
    (Houston       105.00 34.00)
    (Indianapolis   86.10 39.46)
    (Jacksonville   81.40 30.22)
    (Kansas-City    94.35 39.06)
    (Los-Angeles   118.15 34.03)
    (Memphis        90.03 35.09)
    (New-York       73.58 40.47)
    (Oklahoma-City  97.28 35.26)
    (Pittsburgh     79.57 40.27)
    (Quebec         71.11 46.49)
    (Reno          119.49 39.30)
    (San-Francisco 122.26 37.47)
    (Tampa          82.27 27.57)
    (Victoria      123.21 48.25)
    (Wilmington     77.57 34.14)))

(defun neighbors (city)
  "Find all cities within 1000km."
  (find-all-if #'(lambda (c)
                   (and (not (eq c city))
                        (< (air-distance c city) 1000.00)))
               *cities*))

(defun city (name)
  "Find a city with this name."
  (assoc name *cities*))

(defun trip (start dest &optional (beam-width 1))
  "Search for a way from the start to the destination."
  ;;Using a simple beam:
  ;; (beam-search start (is dest) #'neighbors
  ;;              #'(lambda (c) (air-distance c dest))
  ;;              1))
  ;;
  ;;Using beam with path structure
  (beam-search
   (make-path :state start)
   (is dest :key #'path-state)
   (path-saver #'neighbors #'air-distance
               #'(lambda (c) (air-distance c dest)))
   #'path-total-cost
   beam-width))

(defstruct (path (:print-function print-path))
  state (previous nil) (cost-so-far 0) (total-cost 0))

(defun air-distance (city1 city2)
  "The great circle distance between two cities."
  (let ((d (distance (xyz-coords city1) (xyz-coords city2))))
    ;;d is the straight-line chord between the two cities.
    ;;The length of the subtending arc is given by:
    (* earth-diameter (asin (/ d 2)))))

(defun xyz-coords (city)
  "Returns the x,y,z coordinates of a point on a sphere. The center is (0,0,0) and the north pole is (0,0,1)"
  (let ((psi (deg->radians (city-lat city)))
        (phi (deg->radians (city-long city))))
    (list (* (cos psi) (cos phi))
          (* (cos psi) (sin phi))
          (sin psi))))

(defun distance (point1 point2)
  "The Euclidian distance between two points, the points are coordinates in n-dimensional space."
  (sqrt (reduce #'+ (mapcar #'(lambda (a b) (expt (- a b) 2))
                            point1 point2))))

(defun deg->radians (deg)
  (* (+ (truncate deg) (* (rem deg 1) 100/60)) pi 1/180))

(defun path-saver (successors cost-fn cost-left-fn)
  "For each state returned, build up a path that extends the existing one and stores the cost of the path so far as well as the estimated total cost."
  #'(lambda (old-path)
      (let ((old-state (path-state old-path)))
        (mapcar
         #'(lambda (new-state)
             (let ((old-cost (+ (path-cost-so-far old-path)
                                (funcall cost-fn old-state new-state))))
               (make-path :state new-state
                          :previous old-path
                          :cost-so-far old-cost
                          :total-cost (+ old-cost
                                         (funcall cost-left-fn new-state)))))
         (funcall successors old-state)))))

(defun print-path (path &optional (stream t) depth)
  "Print the path without including extra info."
  (declare (ignore depth))
  (format stream "#<Path to ~a cost ~,1f>"
          (path-state path) (path-total-cost path)))

(defun show-city-path (path &optional (stream t))
  "Show the length of a path, and the cities along it."
  (format stream "#<Path ~,1f km: ~{~:(~a~)~^ - ~}>"
          (path-total-cost path)
          (reverse (map-path #'city-name path)))
  (values))

(defun map-path (fn path)
  "Call fn on each state in the path, collecting results."
  (if (null path)
      nil
      (cons (funcall fn (path-state path))
            (map-path fn (path-previous path)))))

(defun iter-wide-search (start goal-p successors cost-fn
                         &key (width 1) (max 100))
  "Search increasing the beam width from width to max. Return the first solution."
  (dbg :search ": Width: ~d" width)
  (unless (> width max)
    (or (beam-search start goal-p successors cost-fn width)
        (iter-wide-search start goal-p successors cost-fn
                          :width (+ width 1) :max max))))

(defun graph-search (states goal-p successors combiner
                     &optional (state= #'eql) old-states)
  "Find a state that satisfies goal-p. Start with states and search according to successors and combiner. Don't try the same state twice."
  (dbg :search "~&;; Search: ~a" states)
  (cond ((null states) fail)
        ((funcall goal-p (first states)) (first states))
        (t (graph-search
            (funcall combiner
                     (new-states states successors state= old-states)
                     (rest states))
            goal-p successors combiner state=
            (adjoin (first states) old-states :test state=)))))

(defun new-states (states successors state= old-states)
  "Generate successor states that have not been seen before."
  (remove-if
   #'(lambda (state)
       (or (member state states :test state=)
           (member state old-states :test state=)))
   (funcall successors (first states))))

(defun next2 (x) (list (+ x 1) (+ x 2)))

(defun a*-search (paths goal-p successors cost-fn cost-left-fn
                  &optional (state-test-fn #'eql) old-paths)
  "Find a path whose state satisfies goal-p. Start with paths, and expand successors, exploring least cost first. When there are duplicate states, keep the one with the lower cost and discard the other."
  (dbg :search ";; Search: ~a" paths)
  (cond
    ((null paths) fail)
    ((funcall goal-p (path-state (first paths)))
     (values (first paths) paths))
    (t (let* ((path (pop paths))
              (state (path-state path)))
         ;;Update PATHS and OLD-PATHS to reflect the new successors of STATE:
         (setf old-paths (insert-path path old-paths))
         (dolist (state2 (funcall successors state))
           (let* ((cost (+ (path-cost-so-far path)
                           (funcall cost-fn state state2)))
                  (cost2 (funcall cost-left-fn state2))
                  (path2 (make-path
                          :state state2 :previous path :cost-so-far cost
                          :total-cost (+ cost cost2)))
                  (old nil))
             (cond
               ((setf old (find-path state2 paths state-test-fn))
                (when (better-path path2 old)
                  (setf paths (insert-path path2 (delete old paths)))))
               ((setf old (find-path state2 old-paths state-test-fn))
                (when (better-path path2 old)
                  (setf paths (insert-path path2 paths))
                  (setf old-paths (delete old old-paths))))
               (t (setf paths (insert-path path2 paths))))))
           ;;Finally call A* again with the updated paths lists:
         (a*-search paths goal-p successors cost-fn cost-left-fn
                      state-test-fn old-paths)))))

(defun insert-path (path paths)
  "Put path into the right position, sorted by total cost."
  (merge 'list (list path) paths #'< :key #'path-total-cost))

(defun find-path (state paths state-test-fn)
  "Find the path with this state among a list of paths."
  (find state paths :key #'path-state :test state-test-fn))

(defun better-path (path1 path2)
  "Is path1 better than path2?"
  (< (path-total-cost path1) (path-total-cost path2)))

(defun path-states (path)
  "Collect the states along this path."
  (if (null path)
      nil
      (cons (path-state path)
            (path-states (path-previous path)))))

(defun search-all (start goal-p successors cost-fn beam-width)
  "Find all the solutions to a search problem using beam-search."
  ;;Careful, can lead to infinite loop
  (let ((solutions nil))
    (beam-search
     start #'(lambda (x)
               (when (funcall goal-p x) (push x solutions))
               nil)
     successors cost-fn beam-width)
    solutions))

(defun search-gps (start goal &optional (beam-width 10))
  "Search for a sequence of operators leading to a goal."
  (find-all-if
   #'action-p
   (beam-search
    (cons '(start) start)
    #'(lambda (state) (subsetp goal state :test #'equal))
    #'gps-successors
    #'(lambda (state)
        (+ (count-if #'action-p state)
           (count-if #'(lambda (con)
                         (not (member-equal con state)))
                     goal)))
    beam-width)))

(defun gps-successors (state)
  "Return a list of states reachable from this one using ops."
  (mapcar
   #'(lambda (op)
       (append
        (remove-if #'(lambda (x)
                       (member-equal x (op-del-list op)))
                   state)
        (op-add-list op)))
   (applicable-ops state)))

(defun applicable-ops (state)
  "Return a list of all ops that are applicable now."
  (find-all-if
   #'(lambda (op)
       (subsetp (op-preconds op) state :test #'equal))
   *ops*))













