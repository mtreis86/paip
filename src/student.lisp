(in-package :paip)

(defstruct (rule (:type list)) pattern response)


(defstruct (exp (:type list)
                (:constructor mkexp (lhs op rhs)))
  op lhs rhs)

(defun exp-p (exp) (consp exp))

(defun exp-args (exp) (rest exp))

(defun prefix->infix (exp)
  "Translate prefix to infix notation."
  (if (atom exp) exp
      (mapcar #'prefix->infix
              (if (binary-exp-p exp)
                  (list (exp-lhs exp) (exp-op exp) (exp-rhs exp))
                  exp))))

(defun binary-exp-p (exp)
  (and (exp-p exp) (= (length (exp-args exp)) 2)))





