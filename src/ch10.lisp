(defun sum-squares (seq)
  (let ((sum 0))
    (dotimes (i (length seq))
      (incf sum (square (elt seq i))))
    sum))

(defun square (x)
  (declare (fixnum x))
  (* x x))

(defun f (x y)
  (declare (fixnum x y) (optimize (speed 3) (safety 0)))
  (the fixnum (+ x y)))

(defun g (x y) (+ x y))

(defmacro defresource (name &key constructor (initial-copies 0)
                              (size (max initial-copies 10)))
  "Take over handling of resources from the system, allowing bypassing of garbage collection routine. WARNING - Don't use this unless necessary."
  (let ((resource (symbol-concat name '-resource))
        (deallocate (symbol-concat 'deallocate- name))
        (allocate (symbol-concat 'allocate- name)))
    `(let ((,resource (make-array ,size :fill-pointer 0)))
       (defun ,allocate ()
         "Get an element from the resource pool, or make one."
         (if (= (fill-pointer ,resource) 0)
             ,constructor
             (vector-pop ,resource)))
       (defun ,deallocate (,name)
         "Place a no-longer-needed element back into the pool."
         (vector-push-extend ,name ,resource))
       ,(when (> initial-copies 0)
          `(mapc #',deallocate (loop repeat ,initial-copies collect (,allocate))))
       ',name)))

(defun symbol-concat (&rest args)
  "Concatenate symbols or strings to form an interned symbol."
  (intern (format nil "~{~a~}" args)))

(defmacro with-resource ((var resource &optional protect) &rest body)
  "Execute body with VAR bound to an instance of RESOURCE."
  (let ((allocate (symbol-concat 'allocate- resource))
        (deallocate (symbol-concat 'deallocate- resource)))
    (if protect
        `(let ((,var nil))
           (unwind-protect
             (progn (setf ,var (,allocate)) ,@body)
             (unless (null ,var) (,deallocate ,var))))
        `(let ((,var (,allocate)))
           ,@body
           (,deallocate ,var)))))


;;Speed comparisons of variable-p options, using sbcl without declaring anything else:

;;Symbol
;;20,670 cycles to test
;;207,883 cycles to make
(defun variable-p-1 (x)
  "Is x a variable (a symbol beginning with '?')?"
  (and (symbolp x) (equal (char (symbol-name x) 0) #\?)))
(defun make-variable-1 ()
  (gentemp "?"))

;;Keyword
(proclaim '(inline variable-p-2 make-variable-2))
;;18,707 cycles to test
;;222,404 cycles to make
(defun variable-p-2 (x)
  (keywordp x))
(defun make-variable-2 ()
  (gentemp "X" #.(find-package "KEYWORD")))

;;Struct and hash-table
;;18,135 cycles to test
;;4,864 cycles to  make
(defstruct (variable-3 (:print-function print-variable-3)) name)
(defvar *vars* (make-hash-table))
(set-macro-character #\?
                     #'(lambda (stream char)
                         ;;find an old var or make a new one with given name
                         (declare (ignore char))
                         (let ((name (read stream t nil t)))
                           (or (gethash name *vars*)
                               (setf (gethash name *vars*)
                                     (make-variable-3 :name name))))))
(defun print-variable-3 (var stream depth)
  (declare (ignore depth))
  (format stream "?~a" (variable-3-name var)))


;;Speed comparisons of persons

;;BROKEN, these don't work:

;;List
(defvar *people-1* nil)
(defstruct person-1 name address id-number)
(defun person-with-id-list (id)
  (find id *people-1* :key #'person-1-id-number))

;;List with pointers
(defvar *people-2* nil)
(defstruct person-2 name address id-number next)
(defun person-with-id-pointer (id)
  (loop for person-2 = *people-2* then (person-2-next person-2)
        until (null person-2)
        do (when (eql id (person-2-id-number person-2))
             (RETURN person-2))))

;;Hash
(defvar *people-3* nil)
(defun person-with-id-hash (id)
  (gethash id *people-3*))


;;Queues
;;ALSO DOESN'T WORK
(proclaim '(inline queue-contents make-queue enqueue dequeue front
            empty-queue-p queue-nconc))

(defun queue-contents (q) (cdr q))

(defun make-queue ()
  "Build a new queue, with no elements."
  (let ((q (cons nil nil)))
    (setf (car q) q)))

(defun enqueue (item q)
  "Insert an item at the end of the queue."
  (setf (car q)
        (setf (rest (car q))
              (cons item nil)))
  q)

(defun dequeue (q)
  "Remove an item from the front of the queue."
  (pop (cdr q))
  (when (null (cdr q))
      (setf (car q) q))
  q)

(defun front (q) (first (queue-contents q)))

(defun empty-queue-p (q) (null (queue-contents q)))

(defun queue-nconc (q list)
  "Add the elements of LIST to the end of the queue."
  (setf (car q)
        (last (setf (rest (car q))
                    list))))


;;trie
(defstruct trie (value nil) (arcs nil))

(defun put-trie (key trie value)
  "Set the value of key in trie."
  (setf (trie-value (find-trie key t trie)) value))

(defun get-trie (key trie)
  "Return the value for a key in trie, and t/nil if found."
  (let* ((key-trie (find-trie key nil trie))
         (val (when key-trie (trie-value key-trie))))
    (if (or (null key-trie)
            (eq val trie-deleted))
        (values nil nil)
        (values val t))))

(defun delete-trie (key trie)
  "Remove a key from the trie."
  (put-trie key trie trie-deleted))

(defun find-trie (key extend trie)
  "Find the trie node for this key. If EXTEND is true, make a new node if need be."
  (cond ((null trie) nil)
        ((atom key)
         (follow-arc key extend trie))
        (t (find-trie
            (cdr key) extend
            (find-trie
             (car key) extend
             (find-trie
              "." extend trie))))))

(defun follow-arc (component extend trie)
  "Find the tree node for this component of the key. If EXTEND is true make a new node if need be."
  (let ((arc (assoc component (trie-arcs trie))))
    (cond ((not (null arc)) (cdr arc))
          ((not extend) nil)
          (t (let ((new-trie (make-trie)))
               (push (cons component new-trie)
                     (trie-arcs trie))
               new-trie)))))





















