(in-package :paip)


(defun unify (x y &optional (bindings no-bindings))
  "See if x and y match with given bindings."
  (cond ((eq bindings fail) fail)
        ((eql x y) bindings)
        ((variable-p x) (unify-variable x y bindings))
        ((variable-p y) (unify-variable y x bindings))
        ((and (consp x) (consp y))
         (unify (rest x) (rest y)
                (unify (first x) (first y) bindings)))
        (t fail)))

(defun unify-variable (var x bindings)
  "Unify var with x, using, and maybe extending, bindings"
  (cond ((get-binding var bindings)
         (unify (lookup var bindings) x bindings))
        ((and (variable-p x) (get-binding x bindings))
         (unify var (lookup x bindings) bindings))
        ((and *occurs-check* (occurs-check var x bindings))
         fail)
        (t (extend-bindings var x bindings))))

(defun occurs-check (var x bindings)
  "Does var occur anywhere inside x?"
  (cond ((eq var x) t)
        ((and (variable-p x) (get-binding x bindings))
         (occurs-check var (lookup x bindings) bindings))
        ((consp x) (or (occurs-check var (first x) bindings)
                       (occurs-check var (rest x) bindings)))
        (t nil)))

(defun subst-bindings (bindings x)
  "Substitute the value of variables in bindings into x, taking recursively bound variables into account."
  (cond ((eq bindings fail) fail)
        ((eq bindings no-bindings) x)
        ((and (variable-p x) (get-binding x bindings))
         (subst-bindings bindings (lookup x bindings)))
        ((atom x) x)
        (t (reuse-cons (subst-bindings bindings (first x))
                       (subst-bindings bindings (rest x))
                       x))))

(defun unifier (x y)
  "Return something that unifies with both x and y, or fails."
  (subst-bindings (unify x y) x))



(defvar *var-counter* 0)

(defvar *trail* (make-array 200 :fill-pointer 0 :adjustable t))

(defstruct (unify-var (:constructor ? ())
                      (:print-function print-var))
  name (incf *var-counter*)
  (binding unbound))

(defun bound-p (var) (not (eq (unify-var-binding var) unbound)))

(defmacro unify-deref (exp)
  "Follow pointers for bound variables."
  `(progn (loop while (and (unify-var-p ,exp) (bound-p ,exp))
                do (setf ,exp (unify-var-binding ,exp)))
          ,exp))

(defun unify! (x y)
  "Destructively unify two expressions."
  (cond ((eql (unify-deref x) (unify-deref y)) t)
        ((unify-var-p x) (set-binding! x y))
        ((unify-var-p y) (set-binding! y x))
        ((and (consp x) (consp y))
         (and (unify! (first x) (first y))
              (unify! (rest x) (rest y))))
        (t nil)))

(defun set-binding! (var value)
  "Set var's binding to value after saving the variable in trail. Always returns t."
  (unless (eq var value)
    (vector-push-extend var *trail*)
    (setf (unify-var-binding var) value))
  t)

(defun print-var (var stream depth)
  (if (or (and (numberp *print-level*))
          (>= depth *print-level*))
      (unify-var-p (deref var)))
  (format stream "?~a" (unify-var-name var))
  (write var :stream stream))

(defun undo-bindings! (old-trail)
  "Undo all bindings back to a given point in the trail."
  (loop until (= (fill-pointer *trail*) old-trail)
        do (setf (unify-var-binding (vector-pop *trail*)) unbound)))







