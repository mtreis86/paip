;;paip examples and exercises chapter 1
;;Michael Reis <michael@mtreis86.com

;; (defun last-name (name)
;;   "Select last name from list"
;;   (first (last name)))


;; (defun first-name (name) 
;;   "Select the first name from a list"
;;   (first name))


;; (setf names '((John Q Public) (Malcom X) (Admiral Major General Grace Hopper) (A A Milne) (Z Z Top) (Sir Larry Oliver) (Miss Scarlet) (Dr Olson MD) (Mr Roberts Sr)))

(defparameter *titles*
  '(Mr Mrs Miss Ms Sir Madam Dr Admiral Major General)
  "A list of titles that can appear at the start of a name")

(defparameter *suffixes*
  '(Jr MD Sr)
  "A list of suffixes that can appear at the end of a name")

(defun first-name (name)
  "Select the first names from a list"
  (if (member (first name) *titles*)
      (first-name (rest name))
      (first name)))

(defun last-name (name)
  "Select the last names from a list"
  (if (member (last name) *suffixes*)
      (last-name (butlast name))
      (last name)))

;;(defun mappend (fn the-list)
;;  "Apply fn to each element of the list and append the results"
;;  (apply #'append (mapcar fn the-list)))

(defun self-and-double (x) (list x (+ x x)))

(defun self-and-inverse (x) (list x (- 0 x)))

(defun number-and-negation (x)
  "if x is a number, return a list of that number and its negation"
  (if (numberp x)
      (list x (- x))
      nil))

(defun numbers-and-negations (input)
  "Given a list return only the numbers and their negatives"
  (mappend #'number-and-negation input))

(defun mappend (fn the-list)
  "Apply fn to each element of the list and append the results"
  (if (null the-list)
      nil
      (append (funcall fn (first the-list))
              (mappend fn (rest the-list)))))

(defun atomprint (exp &optional (depth 0))
  "Print each atom in exp, along with its depth of nesting"
  (if (atom exp)
      (format t "~&ATOM: ~a, DEPTH ~d" exp depth)
      (dolist (element exp)
        (atomprint element (+ depth 1)))))

(defun power (num expt &optional orig)
  "Raise num to the power of expt"
  (if (eq expt 0)
      1
      (if (eq expt 1)
          num
          (if orig
              (power (* num orig) (- expt 1) orig)
              (power (* num num) (- expt 1) num)))))

;;(defun count-atoms (exp &optional (count 0))
;;  "Count the number of atoms in an expression"
;;;;note, this isn't great, tho it works for lists it fails to count a single exp
;;  (if (atom exp)
;;      count
;;      (count-atoms (rest exp) (+ count 1))))

(defun count-atoms (exp)
  "Count the number of atoms in an expression"
  (cond
    ((null exp) 0)
    ((atom exp) 1)
    (t (+ (count-atoms (first exp))
          (count-atoms (rest exp))))))

(defun count-anywhere (item tree)
  "Count the number of time an expression occurs in another expression"
  (cond
    ((eql item tree) 1)
    ((atom tree) 0)
    (t (+ (count-anywhere item (first tree))
          (count-anywhere item (rest tree))))))

;;(defun dot-product (a b)
;;  "Compute the dot product of two lists, multiplying the corresponding elements then adding the resulting products"
;;  (+ (* (first a)
;;        (first b))
;;     (* (first (last a))
;;        (first (last b))))))

(defun dot-product (a b)
  "Compute the dot product of two vectors"
  (apply #'+ (mapcar #'* a b)))

