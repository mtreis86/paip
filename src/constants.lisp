(in-package :paip)
;;paip examples and exercises constants
;;Michael Reis <michael@mtreis86.com



(defconstant fail nil
  "Indicates pat-match failure.")

(defconstant no-bindings '((t . t))
  "Indicates pat-match success, with no variables.")

(defconstant earth-diameter 12765.00
  "Diameter of planet earth in KM.")

(defconstant operators-and-inverses
  '((+ -) (- +) (* /) (/ *) (= =))
  "Operators and their inverse functions.")

(defconstant empty-pipe
  nil
  "An empty pipe.")

(defconstant trie-deleted "Deleted")

(defconstant unbound "Unbound")
