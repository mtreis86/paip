(in-package :paip)


(defun join-if-unique (new old)
  "Map over elements of new, appending them to old if they don't already exist in old. Returns a list of unique elements, in the same order they were inputted, with the two lists being in order of old -> new."
  (declare (optimize (debug 3)))
  (let ((state old))
    (mapcar #'adjoin new state)
    state))
















(defvar *primes* nil)
(setf *primes* '(2 3 5))

(defvar *not-primes* nil)
(setf *not-primes* '(4))

(defun primes (num)
  "Generate primes until multiplying two of them produces the number or no more primes exist to multiply."
  (let ((index 0))
    (loop until (= (length *primes*) index) do
      (print (mapcar #'(lambda (current)
                         (* index current))
                     index))
      (setf index (+ index 1)))))

















































