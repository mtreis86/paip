

;; ;;Non-CLOS objects


;; ;;Dangerous - global variables
;; (defstruct account
;;   (name "") (balance 0.00) (interest-rate 0.06))

;; (defun account-withdraw (account amt)
;;   "Make a withdrawal from this account."
;;   (if (<= (account-balance account) amt)
;;       (decf (account-balance account) amt)
;;       'insufficient-funds))

;; (defun account-deposit (account amt)
;;   "Make a deposit to this account."
;;   (incf (account-balance account) amt))

;; (defun account-interest (account)
;;   "Accumulate interest in this account."
;;   (incf (account-balance account)
;;         (* (account-interest-rate account)
;;            (account-balance account))))

;; ;;Less dangerous using closures
;; (defun new-account (name &optional (balance 0.00)
;;                            (interest-rate 0.06))
;;   "Create an account that knows the following messages:"
;;   #'(lambda (message)
;;       (case message
;;         (withdraw #'(lambda (amt)
;;                       (if (<= amt balance)
;;                           (decf balance amt)
;;                           'insufficient-funds)))
;;         (deposit #'(lambda (amt) (incf balance amt)))
;;         (balance #'(lambda () balance))
;;         (name #'(lambda () name))
;;         (interest #'(lambda ()
;;                       (incf balance
;;                             (* interest-rate balance)))))))

;; (defun get-method (object message)
;;   "Return the method that implements message for this object."
;;   (funcall object message))

;; (defun send (object message &rest args)
;;   "Get the function to implements the message, and apply the message to the args."
;;   (apply (get-method object message) args))

;; ;;Demo of closure account
;; ;; (setf acct (new-account "J. Random Customer" 1000.00))
;; ;; (send acct 'withdraw 500.00)
;; ;; (send acct 'deposit 123.45)
;; ;; (send acct 'name)
;; ;; (send acct 'balance)


;; ;;Basic OOP classes version:
;; (defmacro define-class (class inst-vars class-vars &body methods)
;;   "Define a class for object-oriented programming."
;;   ;;Define constructor and generic functions for methods:
;;   `(let ,class-vars
;;      (mapcar #'ensure-generic-fn ',(mapcar #'first methods))
;;      (defun ,class ,inst-vars
;;        #'(lambda (message)
;;            (case message
;;              ,@(mapcar #'make-clause methods))))))

;; (defun make-clause (clause)
;;   "Translate a message from define-class into a case clause."
;;   `(,(first clause) #'(lambda ,(second clause) .,(rest2 clause))))

;; (defun ensure-generic-fn (message)
;;   "Define an object-oriented dispatch function for a message, unless it has already been defined as one."
;;   (unless (generic-fn-p message)
;;     (let ((fn #'(lambda (object &rest args)
;;                   (apply (get-method object message) args))))
;;       (setf (symbol-function message) fn)
;;       (setf (get message 'generic-fn) fn))))

;; (defun generic-fn-p (fn-name)
;;   "Is this a generic function?"
;;   (and (fboundp fn-name)
;;        (eq (get fn-name 'generic-fn)
;;            (symbol-function fn-name))))

;; (define-class account (name &optional (balance 0.00))
;;     ((interest-rate 0.06))
;;   (withdraw (amt) (if (<= amt balance)
;;                       (decf balance amt)
;;                       'insufficient-funds))
;;   (deposit (amt) (incf balance amt))
;;   (balance () balance)
;;   (name () name)
;;   (interest () (incf balance (* interest-rate balance))))

;; (define-class password-account (password acct) ()
;;   (change-password (pass new-pass)
;;                    (if (equal pass password)
;;                        (setf password new-pass)
;;                        'wrong-password))
;;   (otherwise (pass &rest args)
;;              (if (equal pass password)
;;                  (apply message acct args)
;;                  'wrong-password)))

;; (define-class limited-account (limit acct) ()
;;   (withdraw (amt)
;;             (if (> amt limit)
;;                 'over-limit
;;                 (withdraw acct amt)))
;;   (otherwise (&rest args)
;;              (apply message acct args)))

;; (defstruct (limited-acocunt (:include acount)) limit)

;; (define-class limited-account account (limit) ()
;;   (withdraw (amt)
;;             (if (> amt limit)
;;                 'over-limit
;;                 (call-next-method))))

;; (define-class limited-account-with-password
;;     (password-account limited-account))


;;CLOS Version of accounts:

(defclass account ()
  ((name :initarg :name :reader name)
   (balance :initarg :balance :initform 0.00 :accessor balance)
   (interest-rate :allocation :class :initform 0.06
                  :reader interest-rate)))

(defmethod withdraw ((acct account) amt)
  (if (< amt (balance acct))
      (decf (balance acct) amt)
      'insufficient-funds))

(defclass limited-account (account)
  ((limit :initarg :limit :reader limit)))

(defmethod withdraw ((acct limited-account) amt)
  (if (> amt (limit acct))
      'over-limit
      (call-next-method)))

(defclass audited-account (account)
  ((audit-trail :initform nil :accessor audit-trail)))

(defmethod withdraw :before ((acct audited-account) amt)
  (push (print `(withdrawing ,amt))
        (audit-trail acct)))

(defmethod withdraw :after ((acct audited-account) amt)
  (push (print `(withdrawal (,amt) done))
        (audit-trail acct)))


;;CLOS searching

(defclass problem ()
  ((states :initarg :states :accessor problem-states)))

(defmethod searcher ((prob problem))
  "Find a state that solves the search problem."
  (cond ((no-states-p prob) fail)
        ((goal-p prob) (current-state prob))
        (t (let ((current (pop-state prob)))
             (setf (problem-states prob)
                   (problem-combiner
                    prob
                    (problem-successors prob current)
                    (problem-states prob))))
           (searcher prob))))

(defmethod current-state ((prob problem))
  "The current state is the first of the possible states."
  (first (problem-states prob)))

(defmethod pop-state ((prob problem))
  "Remove and return the current state."
  (pop (problem-states prob)))

(defmethod no-states-p ((prob problem))
  "Are there any more unexplored states?"
  (null (problem-states prob)))

(defmethod searcher :before ((prob problem))
  (dbg 'search "~&;; Search: ~a" (problem-states prob)))

(defclass eql-problem (problem)
  ((goal :initarg :goal :reader problem-goal)))

(defmethod goal-p ((prob eql-problem))
  (eql (current-state prob) (problem-goal prob)))


(defclass dfs-problem (problem) ()
  (:documentation "Depth-first search problem."))

(defclass bfs-problem (problem) ()
  (:documentation "Breadth-first dearch problem."))

(defmethod problem-combiner ((prob dfs-problem) new old)
  "Depth-first search looks at new states first."
  (append new old))

(defmethod problem-combiner ((prob bfs-problem) new old)
  "Breadth-first search looks at old states first."
  (append old new))

;;Binary tree problem
(defclass binary-tree-problem (problem) ())

(defmethod problem-successors ((prob binary-tree-problem) state)
  (let ((n (* 2 state)))
    (list n (+ n 1))))

(defclass binary-tree-eql-bfs-problem
    (binary-tree-problem eql-problem bfs-problem) ())

(defclass best-problem (problem) ()
  (:documentation "Best-first search problem."))

(defmethod problem-combiner ((prob best-problem) new old)
  "Best-first search sorts new and old according to cost-fn."
  (sort (append new old) #'<
        :key #'(lambda (state) (cost-fn prob state))))

(defmethod cost-fn ((prob eql-problem) state)
  (abs (- state (problem-goal prob))))

(defclass beam-problem (problem)
  ((beam-width :initarg :beam-width :initform nil
               :reader problem-beam-width))
  (:documentation "Beam search problem."))

(defmethod problem-combiner :around ((prob beam-problem) new old)
  (let ((next (call-next-method)))
    (subseq next 0 (min (problem-beam-width prob)
                        (length next)))))

(defclass binary-tree-eql-best-beam-problem
    (binary-tree-problem eql-problem best-problem beam-problem) ())


;;Trips problem
(defclass trip-problem (binary-tree-eql-best-beam-problem)
  ((beam-width :initform 1)))

(defmethod cost-fn ((prob trip-problem) city)
  (air-distance (problem-goal prob) city))

(defmethod problem-successors ((prob trip-problem) city)
  (neighbors city))


;;Defining CONC and LEN in CLOS
(defmethod conc ((x null) y) y)

(defmethod conc (x (y null)) x)

(defmethod conc ((x list) (y list))
  (cons (first x) (conc (rest x) y)))

(defmethod conc ((x vector) (y vector))
  (let ((vect (make-array (+ (length x) (length y)))))
    (replace vect x)
    (replace vect y :start1 (length x))))

(defmethod len ((x null)) 0)

(defmethod len ((x cons))
  (+ 1 (len (rest x))))













