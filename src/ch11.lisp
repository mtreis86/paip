

(proclaim '(optimize (debug 3)))
;;(proclaim '(optimize (debug 0) (speed 3) (safety 0)))

(defparameter *occurs-check* t "Should we do the occurs check?")

(defun unify (x y &optional (bindings no-bindings))
  "See if x and y match with given bindings."
  (cond ((eq bindings fail) fail)
        ((eql x y) bindings)
        ((variable-p x) (unify-variable x y bindings))
        ((variable-p y) (unify-variable y x bindings))
        ((and (consp x) (consp y))
         (unify (rest x) (rest y)
                (unify (first x) (first y) bindings)))
        (t fail)))

(defun unify-variable (var x bindings)
  "Unify var with x, using, and maybe extending, bindings"
  (cond ((get-binding var bindings)
         (unify (lookup var bindings) x bindings))
        ((and (variable-p x) (get-binding x bindings))
         (unify var (lookup x bindings) bindings))
        ((and *occurs-check* (occurs-check var x bindings))
         fail)
        (t (extend-bindings var x bindings))))

(defun occurs-check (var x bindings)
  "Does var occur anywhere inside x?"
  (cond ((eq var x) t)
        ((and (variable-p x) (get-binding x bindings))
         (occurs-check var (lookup x bindings) bindings))
        ((consp x) (or (occurs-check var (first x) bindings)
                       (occurs-check var (rest x) bindings)))
        (t nil)))

(defun subst-bindings (bindings x)
  "Substitute the value of variables in bindings into x, taking recursively bound variables into account."
  (cond ((eq bindings fail) fail)
        ((eq bindings no-bindings) x)
        ((and (variable-p x) (get-binding x bindings))
         (subst-bindings bindings (lookup x bindings)))
        ((atom x) x)
        (t (reuse-cons (subst-bindings bindings (first x))
                       (subst-bindings bindings (rest x))
                       x))))

(defun unifier (x y)
  "Return something that unifies with both x and y, or fails."
  (subst-bindings (unify x y) x))


;;PROLOG

;;Clauses are represented by (head . body) cons cells

(defun clause-head (clause) (first clause))

(defun clause-body (clause) (rest clause))

;;Clauses are stored on the predicate's plist

(defun get-clauses (pred) (get pred 'clauses))

(defun predicate (relation) (first relation))

(defvar *db-predicates* nil
  "A list of all predicates stored in the database.")

(defmacro <- (&rest clause)
  "Add a clause to the database."
;;   `(add-clause ',clause))
  `(add-clause ',(replace-?-vars clause)))

(defun add-clause (clause)
  "Add a clause to the database, indexed by head's predicate."
  ;;Predicate must be a non-variable symbol.
  (let ((pred (predicate (clause-head clause))))
    (assert (and (symbolp pred) (not (variable-p pred))))
    (pushnew pred *db-predicates*)
    (setf (get pred 'clauses)
          (nconc (get-clauses pred) (list clause)))
    pred))

(defun clear-db ()
  "Remove all clauses (for all predicates) from the database."
  (mapc #'clear-predicate *db-predicates*))

(defun clear-predicate (predicate)
  "Remove the clause for a single predicate."
  (setf (get predicate 'clauses) nil))

;; (defun prove (goal bindings)
;;   "Return a list of possible solutions to a goal."
;;   (mapcan #'(lambda (clause)
;;               (let ((new-clause (rename-variables clause)))
;;                 (prove-all (clause-body new-clause)
;;                            (unify goal (clause-head new-clause) bindings))))
;;           (get-clauses (predicate goal))))

;; (defun prove-all (goals bindings)
;;   "Return a list of solutions to the conjunction of goals."
;;   (cond ((eq bindings fail) fail)
;;         ((null goals) (list bindings))
;;         (t (mapcan #'(lambda (goal1-solution)
;;                        (prove-all (rest goals) goal1-solution))
;;                    (prove (first goals) bindings)))))

(defun rename-variables (x)
  "Replace all variables in x with new ones."
  (sublis (mapcar #'(lambda (var) (cons var (gensym (string var))))
                  (variables-in x))
          x))

(defun variables-in (exp)
  "Return a list of all the variables in EXP."
  (unique-find-anywhere-if #'variable-p exp))

(defun unique-find-anywhere-if (predicate tree &optional found-so-far)
  "Return a list of leaves of tree satisfying predicate, with dupelicates removed."
  (if (atom tree)
      (if (funcall predicate tree)
          (adjoin tree found-so-far)
          found-so-far)
      (unique-find-anywhere-if
       predicate
       (first tree)
       (unique-find-anywhere-if
        predicate
        (rest tree)
        found-so-far))))

(defmacro ?- (&rest goals) ;;`(prove-all ',goals no-bindings))
;;   `(top-level-prove ',goals))
  `(top-level-prove ',(replace-?-vars goals)))

;; (defun top-level-prove (goals)
;;   "Prove the goals, and print variables readably."
;;   (show-prolog-solutions
;;    (variables-in goals)
;;    (prove-all goals no-bindings)))

;; (defun show-prolog-solutions (vars solutions)
;;   "Print the variables in each of the solutions."
;;   (if (null solutions)
;;       (format t "~&No.")
;;       (mapc #'(lambda (solution) (show-prolog-vars vars solution))
;;             solutions))
;;   (values))

;; (defun show-prolog-vars (vars bindings)
;;   "Print each variable with its binding"
;;   (if (null vars)
;;       (format t "~&Yes")
;;       (dolist (var vars)
;;         (format t "~&~a = ~a" var
;;                 (subst-bindings bindings var))))
;;   (princ ";"))

;; (<- (likes Kim Robin))
;; (<- (likes Sandy Lee))
;; (<- (likes Sandy Kim))
;; (<- (likes Robin cats))
;; (<- (likes Sandy ?x) (likes ?x cats))
;; (<- (likes Kim ?x) (likes ?x Lee) (likes ?x Kim))
;; (<- (likes ?x ?x))


(defun prove-all (goals bindings)
  "Return a list of solutions to the conjunction of goals."
  (cond ((eq bindings fail) fail)
        ((null goals) bindings)
        (t (prove (first goals) bindings (rest goals)))))

;; (defun prove (goal bindings)
;;   "Return a list of possible solutions to a goal."
;;   (some #'(lambda (clause)
;;             (let ((new-clause (rename-variables clause)))
;;               (prove-all
;;                (append (clause-body new-clause) other-goals)
;;                (unify goal (clause-head new-clause) bindings))))
;;         (get-clauses (predicate goal))))

(defun prove (goal bindings other-goals)
  "Return a list of possible solutions to a goal."
  (let ((clauses (get-clauses (predicate goal))))
    (if (listp clauses)
        (some #'(lambda (clause)
                  (let ((new-clause (rename-variables clause)))
                    (prove-all
                     (append (clause-body new-clause) other-goals)
                     (unify goal (clause-head new-clause) bindings))))
              clauses)
        ;;The predicate's clauses can be an atom:
        ;;a primitive function to call
        (funcall clauses (rest goal) bindings
                 other-goals))))

(defun top-level-prove (goals)
  "Prove the goals, and print variables readably."
  (prove-all `(,@goals (show-prolog-vars ,@(variables-in goals)))
             no-bindings)
  (format t "~&No.")
  (values))

(defun show-prolog-vars (vars bindings other-goals)
  "Print each variable with its binding"
  (if (null vars)
      (format t "~&Yes")
      (dolist (var vars)
        (format t "~&~a = ~a" var
                (subst-bindings bindings var))))
  (if (continue-p)
      fail
      (prove-all other-goals bindings)))

(setf (get 'show-prolog-vars 'clauses) 'show-prolog-vars)

(defun continue-p ()
  "As user if we should continue looking for solutions."
  (case (read-char)
    (#\; t)
    (#\. nil)
    (#\newline (continue-p))
    (otherwise
     (format t " Type ; to see more or . to stop")
     (continue-p))))

(defun replace-?-vars (exp)
  "Replace any ? within exp with a var of the form ?123."
  (cond ((eq exp '?) (gensym "?"))
        ((atom exp) exp)
        (t (reuse-cons (replace-?-vars (first exp))
                       (replace-?-vars (rest exp))
                       exp))))


;;Zebra Puzzle
(<- (member ?item (?item . ?rest)))

(<- (member ?item (?x . ?rest)) (member ?item ?rest))

(<- (nextto ?x ?y ?list (iright ?x ?y ?list)))

(<- (nextto ?x ?y ?list (iright ?y ?x ?list)))

(<- (iright ?left ?right (?left ?right . ?rest)))

(<- (iright ?left ?right (?x . ?rest))
    (iright ?left ?right ?rest))

(<- (= ?x ?x))

(<- (zebra ?h ?w ?z)
    ;;House form: (house nationality pet cigarette drink house-color)
    (= ?h ((house norwegian ? ? ? ?)
           ? (house ? ? ? milk ?) ? ?))
    (member (house englishman ? ? ? red) ?h)
    (member (house spaniard dog ? ? ?) ?h)
    (member (house ? ? ? coffee green) ?h)
    (member (house ukrainian ? ? tea ?) ?h)
    (iright (house ? ? ? ? ivory)
            (house ? ? ? ? green) ?h)
    (member (house ? snails winston ? ?) ?h)
    (member (house ? ? kools ? yellow) ?h)
    (nextto (house ? ? chesterfield ? ?)
            (house ? fox ? ? ?) ?h)
    (nextto (house ? ? kools ? ?)
            (house ? horse ? ? ?) ?h)
    (member (house ? ? luckystrike orange-juice ?) ?h)
    (member (house japanese ? parliments ? ?) ?h)
    (nextto (house norwegian ? ? ? ?)
            (house ? ? ? ? blue) ?h)
    ;;Questions
    (member (house ?w ? ? water ?) ?h)
    (member (house ?z ?zebra ? ? ?) ?h))


;;Destructive unification

(defvar *var-counter* 0)

(defstruct (unify-var (:constructor ? ())
                      (:print-function print-var))
  name (incf *var-counter*)
  (binding unbound))

(defun bound-p (var) (not (eq (unify-var-binding var) unbound)))

(defmacro unify-deref (exp)
  "Follow pointers for bound variables."
  `(progn (loop while (and (unify-var-p ,exp) (bound-p ,exp))
                do (setf ,exp (unify-var-binding ,exp)))
          ,exp))

(defun unify! (x y)
  "Destructively unify two expressions."
  (cond ((eql (unify-deref x) (unify-deref y)) t)
        ((unify-var-p x) (set-binding! x y))
        ((unify-var-p y) (set-binding! y x))
        ((and (consp x) (consp y))
         (and (unify! (first x) (first y))
              (unify! (rest x) (rest y))))
        (t nil)))

(defvar *trail* (make-array 200 :fill-pointer 0 :adjustable t))

(defun set-binding! (var value)
;;  "Set var's binding to value. Always succeeds and returns t."
  "Set var's binding to value after saving the variable in trail. Always returns t."
;;  (setf (unify-var-binding var) value)
  (unless (eq var value)
    (vector-push-extend var *trail*)
    (setf (unify-var-binding var) value))
  t)

(defun print-var (var stream depth)
  (if (or (and (numberp *print-level*))
          (>= depth *print-level*))
      (unify-var-p (deref var)))
  (format stream "?~a" (unify-var-name var))
  (write var :stream stream))

(defun undo-bindings! (old-trail)
  "Undo all bindings back to a given point in the trail."
  (loop until (= (fill-pointer *trail*) old-trail)
        do (setf (unify-var-binding (vector-pop *trail*)) unbound)))

















