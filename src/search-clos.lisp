(in-package :paip)

;;Search problems
(defclass problem ()
  ((states :initarg :states :accessor problem-states)))

(defmethod searcher ((prob problem))
  "Find a state that solves the search problem."
  (cond ((no-states-p prob) fail)
        ((goal-p prob) (current-state prob))
        (t (let ((current (pop-state prob)))
             (setf (problem-states prob)
                   (problem-combiner
                    prob
                    (problem-successors prob current)
                    (problem-states prob))))
           (searcher prob))))

(defmethod searcher :before ((prob problem))
  (dbg 'search "~&;; Search: ~a" (problem-states prob)))

(defmethod current-state ((prob problem))
  "The current state is the first of the possible states."
  (first (problem-states prob)))

(defmethod pop-state ((prob problem))
  "Remove and return the current state."
  (pop (problem-states prob)))

(defmethod no-states-p ((prob problem))
  "Are there any more unexplored states?"
  (null (problem-states prob)))


;;Equality goal
(defclass eql-problem (problem)
  ((goal :initarg :goal :reader problem-goal))
  (:documentation "Solution"))

(defmethod goal-p ((prob eql-problem))
  "When problem state equals goal the problem is solved."
  (eql (current-state prob) (problem-goal prob)))


;;Searches

;;Depth-first search
(defclass dfs-problem (problem) ()
  (:documentation "Depth-first search problem."))

(defmethod problem-combiner ((prob dfs-problem) new old)
  "Depth-first search looks at new states first."
  (append new old)())


;;Breadth-first search
(defclass bfs-problem (problem) ()
  (:documentation "Breadth-first dearch problem."))

(defmethod problem-combiner ((prob bfs-problem) new old)
  "Breadth-first search looks at old states first."
  (append old new))


;;Best-first search
(defclass best-problem (problem) ()
  (:documentation "Best-first search problem."))

(defmethod problem-combiner ((prob best-problem) new old)
  "Best-first search sorts new and old according to cost-fn."
  (sort (append new old) #'<
        :key #'(lambda (state) (cost-fn prob state))))

(defmethod cost-fn ((prob eql-problem) state)
  (abs (- state (problem-goal prob)))())


;;Beam-width search
(defclass beam-problem (problem)
  ((beam-width :initarg :beam-width :initform nil
               :reader problem-beam-width))
  (:documentation "Beam search problem."))

(defmethod problem-combiner :around ((prob beam-problem) new old)
  "Beam width searches for the best state in terms of cost-fn but never considers more than beam-width states at a time."
  (let ((next (call-next-method)))
    (subseq next 0 (min (problem-beam-width prob)
                        (length next)))))

#|
TODO finish these
;;Iterative-widening search
(defclass iter-wide-problem (problem)
  ((beam-width :initarg :min-beam-width :initform nil
               :accessor problem-beam-width)
   (max-beam-width :initarg :max-beam-width :initform nil
                   :reader problem-max-beam-width))
  (:documentation "Iterative-widening search problem."))

(defmethod problem-combiner :around ((prob iter-wide-problem) new old)
  "Iterative-widening search takes beam search and increases the beam width for each round."
  (let ((next (call-next-method)))
    (subseq next 0 (min (problem-beam-width prob)
                        (length next)
                        (problem-max-beam-width prob))))
  (setf (problem-beam-width prob) (+ (problem-beam-width prob) 1)))


;;Graph search
(defclass graph-problem (problem) ()
  (:documentation "Graph search problem."))

(defmethod problem-combiner ((prob graph-problem) new old)
  "Graph search looks through old states then new states, but never looks at the same state twice."
  (append-uniques old new))


;;A* search
(defclass a*-problem (problem) ()
  (:documentation "A* search problem."))

(defmethod problem-combiner ((prob a*-problem) new old)
  "A* search explores states with the least cost first, and when there are duplicate states discards the one with higher cost."
  (sort (append-uniques new old) #'<
        :key #'(lambda (state) (cost-fn prob state))))

(defmethod cost-fn ((prob eql-problem) state)
  (abs (- state (problem-goal prob)))())
|#

;;Problems

;;Binary tree problem
(defclass binary-tree-problem (problem) ())

(defmethod problem-successors ((prob binary-tree-problem) state)
  (let ((n (* 2 state)))
    (list n (+ n 1))))

(defclass binary-tree-eql-bfs-problem
   (binary-tree-problem eql-problem bfs-problem) ())

(defclass binary-tree-eql-best-beam-problem
    (binary-tree-problem eql-problem best-problem beam-problem) ())

(defclass binary-tree-eql-graph-problem
    (binary-tree-problem eql-problem graph-problem) ())


;;Next2 problem
(defclass next2-problem (problem) ())

(defmethod problem-successors ((prob next2-problem) state)
  (let ((n (+ state 1)))
    (list n (+ n 1))))

(defclass next2-eql-bfs-problem
    (next2-problem eql-problem bfs-problem) ())

(defclass next2-eql-graph-problem
    (next2-problem eql-problem graph-problem) ())


;;Trips problem
(defclass trip-problem (problem) ())

(defmethod cost-fn ((prob trip-problem) city)
  (air-distance (problem-goal prob) city))

(defmethod problem-successors ((prob trip-problem) city)
  (neighbors city))

(defclass trip-eql-best-beam-problem
    (trip-problem eql-problem best-problem beam-problem)
  ((beam-width :initform 1)))

(defclass trip-eql-a*problem
    (trip-problem eql-problem a*-problem) ())
