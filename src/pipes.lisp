(in-package :paip)

(defun head (pipe) (first pipe))

(defun pipe-elt (pipe i)
  "The i-th element of a pipe, 0-based."
  (if (= i 0)
      (head pipe)
      (pipe-elt (tail pipe) (- i 1))))


(defmacro make-pipe (head tail)
  "Create a pipe by evaluating head and delaying tail."
  `(cons ,head #'(lambda () ,tail)))

(defun tail (pipe)
  "Return tail of pipe or list, and destructively update the tail if it is a function."
  (if (functionp (rest pipe))
      (setf (rest pipe) (funcall (rest pipe)))
      (rest pipe)))

(defun enumerate (pipe &key count key (result pipe))
  "Go through all, or count, the elements of a pipe, possibly applying the KEY function - such as print."
  ;;Returns RESULT which defaults to the pipe itself
  (if (or (eq pipe empty-pipe) (eql count 0))
      result
      (progn
        (unless (null key) (funcall key (head pipe)))
        (enumerate (tail pipe) :count (when count (- count 1))
                   :key key :result result))))

(defun filter (pred pipe)
  "Keep only items in the pipe satisfying pred."
  (if (funcall pred (head pipe))
      (make-pipe (head pipe) (filter pred (tail pipe)))
      (filter pred (tail pipe))))















