

(defun prolog-compile (symbol &optional
                                (clauses (get-clauses symbol)))
  "Compile a symbol; make a separate function for each arity."
  (unless (null clauses)
    (let ((arity (relation-arity (clause-head (first clauses)))))
      ;;Compile the clauses with this arity.
      (compile-predicate
       symbol arity (clauses-with-arity clauses #'= arity))
      ;;Compile all the clauses with any other arity.
      (prolog-compile
       symbol (clauses-with-arity clauses #'/= arity)))))

(defun clauses-with-arity (clauses test arity)
  "Return all clauses whose head has given arity."
  (find-all arity clauses
            :key #'(lambda (clause)
                     (relation-arity (clause-head clause)))
            :test test))

(defun relation-arity (relation)
  "The number of arguments to a relation. Example: (relation-arity '(p a b c) -> 3)"
  (length (args relation)))

(defun args (relation)
  "The arguments of a relation."
 (rest relation))

(defun compile-predicate (symbol arity clauses)
  "Compile all the clauses for a given symbol/arity into a single LISP function."
  (let ((predicate (make-predicate symbol arity))
        (parameters (make-parameters arity)))
    (compile
     (eval
      `(defun ,predicate (,@parameters cont)
         .,(mapcar #'(lambda (clause)
                       (compile-clause parameters clause 'cont))
                     clauses))))))

(defun make-parameters (arity)
  "Return the list (?arg1 ?arg2 ... ?arg-arity)."
  (loop for i from 1 to arity
        collect (new-symbol '?arg i)))

(defun make-predicate (symbol arity)
  "Return the symbol: symbol/arity."
  (symbol-concat symbol '/ arity))

(defun compile-clause (parms clause cont)
  "Transform away the head, and compile the resulting body"
  (compile-body
   (nconc
    (mapcar #'make-= parms (args (clause-head clause)))
    (clause-body clause))
   cont))

(defun make-= (x y) `(= ,x ,y))

(defun compile-body (body cont)
  "Compile the body of a clause."
  ())











