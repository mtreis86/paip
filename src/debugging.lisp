(in-package :paip)

(defvar *dbg-ids* nil "Identifiers used by dbg")

(defun dbg (id format-string &rest args)
  "Print debugging info if (DEBUG ID) has been specified"
  (when (member id *dbg-ids*)
    (fresh-line *debug-io*)
    (apply #'format *debug-io* format-string args)))

(defun start-debugging (&rest ids)
  "Start debugging output on the given ids"
  (setf *dbg-ids* (union ids *dbg-ids*)))

(defun stop-debugging (&rest ids)
  "stop dbg on the ids. With no ids, stop dbg alltogether"
  (setf *dbg-ids* (if (null ids) nil
                      (set-difference *dbg-ids* ids)))

 (defun dbg-indent (id indent format-string &rest args)
  "Print indented debugging info if (DEBUG ID) has been specified"
  (when (member id *dbg-ids*)
    (fresh-line *debug-io*)
    (dotimes (i indent) (princ "  " *debug-io*))
    (apply #'format *debug-io* format-string args))))

(defun profile-count (fn-name) (get fn-name 'profile-count))

(defvar *profiled-functions* nil
       "Functions that are being profiled.")

(defmacro start-profile (&rest fn-names)
  "Profile fn-names. With no args, list profiled functions."
  `(mapcar #'profile1
           (setf *profiled-functions*
                 (union *profiled-functions* ',fn-names))))

(defmacro stop-profile (&rest fn-names)
  "Stop profiling fn-names. With no args, stop all profiling."
  `(progn
     (mapcar #'unprofile1
             ,(if fn-names `',fn-names '*profiled-functions*))
     (setf *profiled-functions*
           ,(if (null fn-names)
                nil
                `(set-difference *profiled-functions*
                                 ',fn-names)))))

(defun profile1 (fn-name)
  "Make the function count how often it is called."
  ;;First save away the old unprofiled function
  ;;Then make the name be a new function that increments
  ;;a counter and then calls the original function
  (let ((fn (symbol-function fn-name)))
    (unless (eq fn (get fn-name 'profiled-fn))
      (let ((new-fn (profiled-fn fn-name fn)))
        (setf (symbol-function fn-name) new-fn
              (get fn-name 'profiled-fn) new-fn
              (get fn-name 'unprofiled-fn) fn
              (get fn-name 'profile-time) 0
              (get fn-name 'profile-count) 0))))
  fn-name)

(defun unprofile1 (fn-name)
  "Make the function stop counting how often it is called."
  (setf (get fn-name 'profile-time) 0)
  (setf (get fn-name 'profile-count) 0)
  (when (eq (symbol-function fn-name) (get fn-name 'profiled-fn))
    ;;Normal case: restore unprofiled fn:
    (setf (symbol-function fn-name)
          (get fn-name 'unprofiled-fn)))
  fn-name)

(defun get-fast-time ()
  "Return elapsed time, this may wrap around. Use fast-time-difference to compare."
  (get-internal-real-time))

(defun fast-time-difference (end start)
  "Subtract two time points."
  (- end start))

(defun fast-time->seconds (time)
  "Convert fast time interval to seconds"
  (/ time internal-time-units-per-second))

(proclaim '(inline profile-enter profile-exit inc-profile-time))

(defun profiled-fn (fn-name fn)
  "Return a function that increments the count."
  #'(lambda (&rest args)
      (profile-enter fn-name)
      (multiple-value-prog1
          (apply fn args)
        (profile-exit fn-name))))

(defvar *profile-call-stack* nil)

(defun profile-enter (fn-name)
  (incf (get fn-name 'profile-count))
  (unless (null *profile-call-stack*)
    ;;Time charged against the calling function:
    (inc-profile-time (first *profile-call-stack*)
                      (first (first *profile-call-stack*))))
  ;;Put a new entry on the stack:
  (push (cons fn-name (get-fast-time))
        *profile-call-stack*))

(defun profile-exit (fn-name)
  ;;Time charged against the current function:
  (inc-profile-time (pop *profile-call-stack*) fn-name)
  ;;Change the top entry to reflect the current time:
  (unless (null *profile-call-stack*)
    (setf (rest (first *profile-call-stack*))
          (get-fast-time))))

(defun inc-profile-time (entry fn-name)
  (incf (get fn-name 'profile-time)
        (fast-time-difference (get-fast-time) (rest entry))))

(defun profile-report (&optional
                         (fn-names (copy-list *profiled-functions*))
                         (key #'profile-count))
  "Report profiling statistics on given functions."
  (declare (optimize (debug 3)))
  (let ((total-time (reduce #'+ (mapcar #'profile-time fn-names))))
    (unless (null key)
      (setf fn-names (sort fn-names #'> :key key)))
    (format t "~&Total elapsed time: ~,3F seconds."
            (fast-time->seconds total-time))
    (format t "~&  Count   Secs Time% Name")
    (loop for name in fn-names do
      (format t "~&~7D ~6,4F  ~3d% ~A"
              (profile-count name)
              (fast-time->seconds (profile-time name))
              (time-percent (profile-time name) total-time)
              ;;              (round (/ (profile-time name) total-time) .01)
              name))))

(defun time-percent (time-part time-total)
  "Calculate percentage to 0.01 while avoiding divide-by-zero errors."
  (if (not (= time-total 0))
      (round (/ time-part time-total) .01)
      0))

(defun profile-time (fn-name) (get fn-name 'profile-time))

(defmacro with-profiling (fn-names &rest body)
  `(progn
     (stop-profile . ,fn-names)
     (start-profile . ,fn-names)
     (setf *profile-call-stack* nil)
     (unwind-protect
          (progn . ,body)
       (profile-report ',fn-names)
       (stop-profile . ,fn-names))))

