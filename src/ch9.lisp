;;paip examples and exercises chapter 9
;;Michael Reis <michael@mtreis86.com


(proclaim '(optimize (debug 3)))

(defun fib (n)
  "Compute the nth number in the Fibonacci sequence."
  (if (<= n 1) 1
      (+ (fib (- n 1))
         (fib (- n 2)))))

;; (defun memo (fn)
;;   "Return a memoized version of the function."
;;   (let ((table (make-hash-table)))
;;     #'(lambda (x)
;;         (multiple-value-bind (val found-p)
;;             (gethash x table)
;;           (if found-p
;;               val
;;               (setf (gethash x table) (funcall fn x)))))))

;; (defun memoize (fn-name)
;;   "Replace fn-name's global definition with the memoized version."
;;   (setf (symbol-function fn-name) (memo (symbol-function fn-name))))

(defmacro defun-memo (fn args &body body)
  "Define a memoized function."
  `(memoize (defun ,fn ,args . ,body)))

(defun-memo fib-memo (n)
  "Compute the nth number in the Fibonacci sequence. Memoized version."
  (if (<= n 1) 1
      (+ (fib-memo (- n 1))
         (fib-memo (- n 2)))))


(defun memo (fn name key test)
  "Return a memoized version of the function."
  (let ((table (make-hash-table :test test)))
    (setf (get name 'memo) table)
    #'(lambda (&rest args)
        (let ((k (funcall key args)))
          (multiple-value-bind (val found-p)
              (gethash k table)
            (if found-p val
              (setf (gethash k table) (apply fn args))))))))

(defun memoize (fn-name &key (key #'first) (test #'eql))
  "Replace fn-name's global definition with the memoized version."
  (setf (symbol-function fn-name)
        (memo (symbol-function fn-name) fn-name key test)))

(defun clear-memoize (fn-name)
  "Clear the hash table from a memo function."
  (let ((table (get fn-name 'memo)))
    (when table (clrhash table))))

(defun rule-lhs (rule)
  "The left-hand side of a rule."
  (first rule))

(defun rule-rhs (rule)
  "The right-hand side of a rule."
  (cdr rule))

(defun one-of (set)
  "Pick one element of a set, and make a list of it."
  (list (random-elt set)))

(defun random-elt (choices)
  "Pick an element from a list at random."
  (elt choices (random (length choices))))

;; (defun compile-rule (rule)
;;   "Translate a grammar rule into a LISP function definition."
;;   (let ((rhs (rule-rhs rule)))
;;     `(defun ,(rule-lhs rule) ()
;;        ,(cond ((every #'atom rhs) `(one-of ',rhs))
;;               (t `(case (random ,(length rhs))
;;                    ,@(build-cases 0 rhs)))))))

(defun build-cases (number choices)
  "Return a list of case-clauses."
  (when choices
    (cons (list number (build-code (first choices)))
          (build-cases (+ number 1) (rest choices)))))

(defun build-code (choice)
  "Append together multiple constituents."
  (cond ((null choice) nil)
        ((atom choice) (list choice))
        ((length=1 choice) choice)
        (t `(append ,@(mapcar #'build-code choice)))))

(defun length=1 (x)
  "Is x a list of length 1?"
  (and (consp x) (null (rest x))))

;;use cases:
;;interpreted (easier to debug):
;; (dolist (rule *grammar*) (eval (compile-rule rule)))
;;compiled:
;; (dolist (rule *grammar*) (compile (eval (compile-rule rule))))

(defmacro defrule (&rest rule)
  "Define a grammar rule."
  (compile-rule rule))

(defstruct delay (value nil) (function nil))

(defmacro delay (&rest body)
  "A computation that can be executed later by FORCE."
  `(make-delay :function #'(lambda () . ,body)))

(defun force (x)
  "Find the value of x, by computing if it is a delay."
  (if (not (delay-p x))
      x
      (progn
        (when (delay-function x)
          (setf (delay-value x) (funcall (delay-function x)))
          (setf (delay-function x) nil))
        (delay-value x))))

;; (defmacro make-pipe (head tail)
;;   "Create a pipe by evaluating head and delaying tail."
;;   `(cons ,head (delay ,tail)))

(defun head (pipe) (first pipe))
;; (defun tail (pipe) (force (rest pipe)))

(defun pipe-elt (pipe i)
  "The i-th element of a pipe, 0-based."
  (if (= i 0)
      (head pipe)
      (pipe-elt (tail pipe) (- i 1))))

;;use case for pipe:
;; (defun integers (&optional (start 0) end)
;;   "A pipe of integers from START to END. If END is nil, this is an infinite pipe."
;;   (if (or (null end) (<= start end))
;;       (make-pipe start (integers (+ start 1) end))
;;       nil))

(defmacro make-pipe (head tail)
  "Create a pipe by evaluating head and delaying tail."
  `(cons ,head #'(lambda () ,tail)))

(defun tail (pipe)
  "Return tail of pipe or list, and destructively update the tail if it is a function."
  (if (functionp (rest pipe))
      (setf (rest pipe) (funcall (rest pipe)))
      (rest pipe)))

(defun enumerate (pipe &key count key (result pipe))
  "Go through all, or count, the elements of a pipe, possibly applying the KEY function - such as print."
  ;;Returns RESULT which defaults to the pipe itself
  (if (or (eq pipe empty-pipe) (eql count 0))
      result
      (progn
        (unless (null key) (funcall key (head pipe)))
        (enumerate (tail pipe) :count (when count (- count 1))
                   :key key :result result))))

(defun filter (pred pipe)
  "Keep only items in the pipe satisfying pred."
  (if (funcall pred (head pipe))
      (make-pipe (head pipe) (filter pred (tail pipe)))
      (filter pred (tail pipe))))


;;Pipes in use to generate primes:
(defun sieve (pipe)
  (make-pipe (head pipe)
             (filter #'(lambda (x) (/= (mod x (head pipe)) 0))
                     (sieve (tail pipe)))))

;; (defvar *primes* (sieve (integers 2)))

;;Pipes in use to generate grammar strings:
;;Utility functions:
(defun map-pipe (fn pipe)
  "Map fn over pipe delaying all but the first fn call"
  (if (eq pipe empty-pipe)
      empty-pipe
      (make-pipe (funcall fn (head pipe))
                 (map-pipe fn (tail pipe)))))

(defun append-pipes (x y)
  "Return a pipe that appends the elements of x and y."
  (if (eq x empty-pipe)
      y
      (make-pipe (head x)
                 (append-pipes (tail x) y))))

(defun mappend-pipe (fn pipe)
  "Lazily map fn over pipe, appending the results."
  (if (eq pipe empty-pipe)
      empty-pipe
      (let ((x (funcall fn (head pipe))))
        (make-pipe (head x)
                   (append-pipes (tail x)
                                 (mappend-pipe fn (tail pipe)))))))

;;Actual generation of grammar:
(defun generate-all (phrase)
  "Generate a random phrase or sentence."
  (if (listp phrase)
      (if (null phrase)
          (list nil)
          (combine-all-pipes
           (generate-all (first phrase))
           (generate-all (rest phrase))))
      (let ((choices (rule-rhs (assoc phrase *grammar*))))
        (if choices
            (mappend-pipe #'generate-all choices)
            (list (list phrase))))))

(defun combine-all-pipes (xpipe ypipe)
  "Return a pipe of pipes formed by appending y to x."
  ;;Form the cartesian product
  (mappend-pipe
   #'(lambda (y)
       (map-pipe #'(lambda (x)
                     (append-pipes x y))

                 xpipe))
   ypipe))


;;Profiling

;; (defun profile1 (fn-name)
;;   "Make the function count how often it is called."
;;   ;;First save away the old unprofiled function
;;   ;;Then make the name be a new function that increments
;;   ;;a counter and then calls the original function
;;   (let ((fn (symbol-function fn-name)))
;;     (setf (get fn-name 'unprofiles-fn) fn)
;;     (setf (get fn-name 'profile-count) 0)
;;     (setf (symbol-function fn-name)
;;           (profiled-fn fn-name fn))
;;     fn-name))

;; (defun unprofile1 (fn-name)
;;   "Make the function stop counting how often it is called."
;;   (setf (symbol-function fn-name) (get fn-name 'unprofiled-fn))
;;   fn-name)

;; (defun profiled-fn (fn-name fn)
;;   "Return a function that increments the count."
;;   #'(lambda (&rest args)
;;       (incf (get fn-name 'profile-count))
;;       (apply fn args)))

(defun profile-count (fn-name) (get fn-name 'profile-count))

;; (defun profile-report (fn-names &optional (key #'profile-count))
;;   "Report profiling stats on given functions."
;;   (loop for name in (sort fn-names #'> :key key) do
;;         (format t "~&~7D ~A" (profile-count name) name)))

(defvar *profiled-functions* nil
       "Functions that are being profiled.")

(defmacro start-profile (&rest fn-names)
  "Profile fn-names. With no args, list profiled functions."
  `(mapcar #'profile1
           (setf *profiled-functions*
                 (union *profiled-functions* ',fn-names))))

(defmacro stop-profile (&rest fn-names)
  "Stop profiling fn-names. With no args, stop all profiling."
  `(progn
     (mapcar #'unprofile1
             ,(if fn-names `',fn-names '*profiled-functions*))
     (setf *profiled-functions*
           ,(if (null fn-names)
                nil
                `(set-difference *profiled-functions*
                                 ',fn-names)))))

(defun profile1 (fn-name)
  "Make the function count how often it is called."
  ;;First save away the old unprofiled function
  ;;Then make the name be a new function that increments
  ;;a counter and then calls the original function
  (let ((fn (symbol-function fn-name)))
    (unless (eq fn (get fn-name 'profiled-fn))
      (let ((new-fn (profiled-fn fn-name fn)))
        (setf (symbol-function fn-name) new-fn
              (get fn-name 'profiled-fn) new-fn
              (get fn-name 'unprofiled-fn) fn
              (get fn-name 'profile-time) 0
              (get fn-name 'profile-count) 0))))
  fn-name)

(defun unprofile1 (fn-name)
  "Make the function stop counting how often it is called."
  (setf (get fn-name 'profile-time) 0)
  (setf (get fn-name 'profile-count) 0)
  (when (eq (symbol-function fn-name) (get fn-name 'profiled-fn))
    ;;Normal case: restore unprofiled fn:
    (setf (symbol-function fn-name)
          (get fn-name 'unprofiled-fn)))
  fn-name)

(defun get-fast-time ()
  "Return elapsed time, this may wrap around. Use fast-time-difference to compare."
  (get-internal-real-time))

(defun fast-time-difference (end start)
  "Subtract two time points."
  (- end start))

(defun fast-time->seconds (time)
  "Convert fast time interval to seconds"
  (/ time internal-time-units-per-second))

(proclaim '(inline profile-enter profile-exit inc-profile-time))

(defun profiled-fn (fn-name fn)
  "Return a function that increments the count."
  #'(lambda (&rest args)
      (profile-enter fn-name)
      (multiple-value-prog1
          (apply fn args)
        (profile-exit fn-name))))

(defvar *profile-call-stack* nil)

(defun profile-enter (fn-name)
  (incf (get fn-name 'profile-count))
  (unless (null *profile-call-stack*)
    ;;Time charged against the calling function:
    (inc-profile-time (first *profile-call-stack*)
                      (first (first *profile-call-stack*))))
  ;;Put a new entry on the stack:
  (push (cons fn-name (get-fast-time))
        *profile-call-stack*))

(defun profile-exit (fn-name)
  ;;Time charged against the current function:
  (inc-profile-time (pop *profile-call-stack*) fn-name)
  ;;Change the top entry to reflect the current time:
  (unless (null *profile-call-stack*)
    (setf (rest (first *profile-call-stack*))
          (get-fast-time))))

(defun inc-profile-time (entry fn-name)
  (incf (get fn-name 'profile-time)
        (fast-time-difference (get-fast-time) (rest entry))))

(defun profile-report (&optional
                         (fn-names (copy-list *profiled-functions*))
                         (key #'profile-count))
  "Report profiling statistics on given functions."
  (declare (optimize (debug 3)))
  (let ((total-time (reduce #'+ (mapcar #'profile-time fn-names))))
    (unless (null key)
      (setf fn-names (sort fn-names #'> :key key)))
    (format t "~&Total elapsed time: ~,3F seconds."
            (fast-time->seconds total-time))
    (format t "~&  Count   Secs Time% Name")
    (loop for name in fn-names do
      (format t "~&~7D ~6,4F  ~3d% ~A"
              (profile-count name)
              (fast-time->seconds (profile-time name))
              (time-percent (profile-time name) total-time)
              ;;              (round (/ (profile-time name) total-time) .01)
              name))))

(defun time-percent (time-part time-total)
  "Calculate percentage to 0.01 while avoiding divide-by-zero errors."
  (if (not (= time-total 0))
      (round (/ time-part time-total) .01)
      0))

(defun profile-time (fn-name) (get fn-name 'profile-time))

(defmacro with-profiling (fn-names &rest body)
  `(progn
     (stop-profile . ,fn-names)
     (start-profile . ,fn-names)
     (setf *profile-call-stack* nil)
     (unwind-protect
          (progn . ,body)
       (profile-report ',fn-names)
       (stop-profile . ,fn-names))))

;;Simplify part two

(defvar *test-data*
  (mapcar #'infix->prefix
          '((d (a * x ^ 2 + b * x + c) / d x)
            (d ((a * x ^ 2 + b * x + c * x + d) / x ^ 5) / d x)
            ((sin (x + x)) * (sin (2 * x)) + (cos (d (x ^ 2) / d x )) ^ 1)
            (d (3 * x + (cos x) / x) / d x))))

(defvar *answers* (mapcar #'simplify *test-data*))

(defun test-it (&optional (with-profiling t))
  "Time a test run and make sure the answers are correct."
  (let ((answers
          (if with-profiling
              (with-profiling (simplify simplify-exp pat-match
                                        match-variable variable-p)
                (mapcar #'simplify *test-data*))
              (time (mapcar #'simplify *test-data*)))))
    (mapc #'assert-equal answers *answers*)
    t))

(defun assert-equal (x y)
  "If x is not equal to y, complain."
  (assert (equal x y) (x y)
          "Expected ~a to be equal to ~a" x y))

(defun simplify-exp (exp)
  "Simplify using a rule, or by doing arithmetic, or by using the simp function provided by this operator. This version indexes simplification rules under the operator."
  (cond ((simplify-by-fn exp))
        ((rule-based-translator exp (rules-for (exp-op exp))
                                :rule-if #'exp-lhs :rule-then #'exp-rhs
                                :action #'(lambda (bindings response)
                                            (simplify
                                             (sublis bindings response)))))
        ((evaluable exp) (eval exp))
        (t exp)))

(defvar *rules-for* (make-hash-table :test #'eq))

(defun main-op (rule) (exp-op (exp-lhs rule)))

(defun index-rules (rules)
  "Index all the rules under the main op."
  (clrhash *rules-for*)
  (dolist (rule rules)
    ;;nconc instead of push to preserve the order of rules.
    (setf (gethash (main-op rule) *rules-for*)
          (nconc (gethash (main-op rule) *rules-for*)
                 (list rule)))))

(defun rules-for (op) (gethash op *rules-for*))

(index-rules *simplification-rules*)

;; (memoize 'simplify :test #'equal)

(defvar *bindings* nil "A list of bindings used by the compiler.")

(defun compile-rule (rule)
  "Compile a single rule."
  (let ((*bindings* nil))
    `(lambda (x)
       ,(compile-exp 'x (exp-lhs rule)
                     (delay (build-exp (exp-rhs rule) *bindings*))))))

(defun compile-exp (var pattern consequent)
  "Compile code that tests the expression, and does consequent if it matches. Assume the bindings in *bindings*."
  (cond
    ;;Test a previously bound variable:
    ((get-binding pattern *bindings*)
     `(if (equal ,var ,(lookup pattern *bindings*))
          ,(force consequent)))
    ;;Add a new binding; do type checking if needed:
    ((variable-p pattern)
     (push (cons pattern var) *bindings*)
     (force consequent))
    ;;Match a literal atom:
    ((atom pattern)
     `(if (eql ,var ',pattern)
          ,(force consequent)))
    ;;Rules:
    ((starts-with pattern '?is)
     (push (cons (second pattern) var) *bindings*)
     `(if (,(third pattern) ,var)
          ,(force consequent)))
    ;;Add code here to compile for other rules:
    ;;Check the operator and arguements
    (t `(if (op? ,var ',(exp-op pattern))
            ,(compile-args var pattern consequent)))))

(defun compile-args (var pattern consequent)
  "Compile code that checks the arg or args, and does consequent if the arg(s) match."
  ;;Make up variable names for the args:
  (let ((L (symbol-concat var 'L))
        (R (symbol-concat var 'R)))
    (if (exp-rhs pattern)
        ;;Two arg case:
        `(let ((,L (exp-lhs ,var))
               (,R (exp-rhs ,var)))
           ,(compile-exp L (exp-lhs pattern)
                         (delay
                          (compile-exp R (exp-rhs pattern)
                                       consequent))))
        ;;One arg case:
        `(let ((,L (exp-lhs ,var)))
           ,(compile-exp L (exp-lhs pattern) consequent)))))

(defun build-exp (exp bindings)
  "Compile code that will build the exp, given the bindings."
  (cond ((assoc exp bindings) (rest (assoc exp bindings)))
        ((variable-p exp)
         (error "Variable ~a occured on the right-hand
                 side but not left." exp))
        ((atom exp) `',exp)
        (t (let ((new-exp (mapcar #'(lambda (x)
                                      (build-exp x bindings))
                                  exp)))
             `(simplify-exp (list .,new-exp))))))

(defun op? (exp op)
  "Does the exp have the given op as its operator?"
  (and (exp-p exp) (eq (exp-op exp) op)))

(defun symbol-concat (&rest args)
  "Concatenate symbols or strings to form an interned symbol."
  (intern (format nil "~{~a~}" args)))

(defun new-symbol (&rest args)
  "Concatenate symbol or strings to form an uninterned symbol."
  (make-symbol (format nil "~{~a~}" args)))

;; (defun compile-rule (rule)
;;   "Translate a grammar rule into a LISP function definition."
;;   (let ((rhs (rule-rhs rule)))
;;     `(defun ,(rule-lhs rule) ()
;;        ,(cond ((every #'atom rhs) `(one-of ',rhs))
;;               (t `(case (random ,(length rhs))
;;                    ,@(build-cases 0 rhs)))))))

(defun compile-rule-set (op)
  "Compile all rules indexed under a given main op, and make them into the simp-fn for that op."
  (set-simp-fn op
               (compile nil
                        `(lambda (x)
                           ,(reduce #'combine-rules
                                      (mapcar #'compile-indexed-rule
                                                (rules-for op)))))))

(defun compile-indexed-rule (rule)
  "Compile one rule into lambda-less code. Assume indexing of main op."
  (let ((*bindings* nil))
    (compile-args
     'x (exp-lhs rule)
      (delay (build-exp (exp-rhs rule) *bindings*)))))

(defun combine-rules (a b)
  "Combine the code for two rules into one, maintaining order."
  ;;In the default case, generate the code (or a b),
  ;;but try to be clever and share common code,
  ;;assuming no side effects.
  (cond ((and (listp a) (listp b)
              (= (length a) (length b) 3)
              (equal (first a) (first b))
              (equal (second a) (second b)))
         ;;a=(f x y), b=(f x z) -> (f x (combine-rules (y z))),
         ;;can only apply if f=IF or f=LET
         (list (first a) (second a)
               (combine-rules (third a) (third b))))
        ((matching-ifs a b)
         `(if ,(second a)
              ,(combine-rules (third a) (third b))
              ,(combine-rules (fourth a) (fourth b))))
         ;;a=(or ... (if p y)), b=(if p z) ->
         ;;    (or ... (if p (combine-rules y z)))
         ;;  else a=(r ...) b -> (or ... b)
        ((starts-with a 'or)
         (if (matching-ifs (last1 a) b)
             (append (butlast a)
                     (list (combine-rules (last1 a) b)))
             (append a (list b))))
        ;;a, b -> (or a b)
        (t `(or ,a ,b))))

(defun matching-ifs (a b)
  "Are a and b if statements with the same predicate?"
  (and (starts-with a 'if) (starts-with b 'if)
       (equal (second a) (second b))))

(defun last1 (list)
  "Return the last element of a list but not the cons cell."
  (first (last list)))

(defun compile-all-rules-indexed (rules)
  "Compile a separate fn for each operator, and store it as the simp-fn of that operator."
  (index-rules rules)
  (let ((all-ops (delete-duplicates (mapcar #'main-op rules))))
    (mapc #'compile-rule-set all-ops)))

(compile-all-rules-indexed *simplification-rules*)
