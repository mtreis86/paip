;;paip examples and exercises chapter 7
;;Michael Reis <michael@mtreis86.com

(defstruct (exp (:type list)
                (:constructor mkexp (lhs op rhs)))
  op lhs rhs)

(defun exp-p (x) (consp x))

(defun exp-args (x) (rest x))

(defun rule-pattern (rule) (first rule))

(defun rule-response (rule) (rest rule))

(pat-match-abbrev '?x* '(?* ?x))

(pat-match-abbrev '?y* '(?* ?y))

(defparameter *student-rules*
  (mapcar #'expand-pat-match-abbrev
    '(((?x* \.)               ?x)
      ((?x* \. ?y*)          (?x ?y))
      ((if ?x* \, then ?y*)  (?x ?y))
      ((if ?x* then ?y*)     (?x ?y))
      ((if ?x* \, ?y*)       (?x ?y))
      ((?x* \, and ?y*)      (?x ?y))
      ((find ?x* and )       ((= to-find-1 ?x) (= to-find-2 ?y)))
      ((find ?x*)            (= to-find ?x))
      ((?x* equal ?y*)       (= ?x ?y))
      ((?x* same as ?y*)     (= ?x ?y))
      ((?x* = ?y*)           (= ?x ?y))
      ((?x* is equal to ?y*) (= ?x ?y))
      ((?x* is ?y*)          (= ?x ?y))
      ((?x* - ?y*)           (- ?x ?y))
      ((?x* minus ?y*)       (- ?x ?y))
      ((difference between ?x* and ?y*)  (- ?y ?x))
      ((difference ?x* and ?y*)          (- ?y ?x))
      ((?x* + ?y*)            (+ ?x ?y))
      ((?x* plus ?y*)         (+ ?x ?y))
      ((sum ?x* and ?y*)      (+ ?x ?y))
      ((product ?x* and ?y*)  (* ?x ?y))
      ((?x* * ?y*)            (* ?x ?y))
      ((?x* times ?y*)        (* ?x ?y))
      ((?x* / ?y*)            (/ ?x ?y))
      ((?x* per ?y*)          (/ ?x ?y))
      ((?x* divided by ?y*)   (/ ?x ?y))
      ((half ?x*)             (/ ?x 2))
      ((one half ?x*)         (/ ?x 2))
      ((twice ?x*)            (* 2 ?x))
      ((square root of ?x*)   (sqrt ?x))
      ((square ?x*)           (* ?x ?x))
      ((?x* squared)          (* ?x ?x))
      ((?x* % less than ?y*)  (* ?y (/ (- 100 ?x) 100)))
      ((?x* % more than ?y*)  (* ?y (/ (+ 100 ?x) 100)))
      ((?x* % ?y*)            (* (/ ?x 100) ?y)))))

(defun student (words)
  "Solve certain algebra word problems."
  (solve-equations
   (create-list-of-equations
    (translate-to-expression (remove-if #'noise-word-p words)))))

(defun translate-to-expression (words)
  "Translate and english phrase into an equation or expression."
  (or (rule-based-translator
       words *student-rules*
       :rule-if #'rule-pattern :rule-then #'rule-response
       :action #'(lambda (bindings response)
                   (sublis (mapcar #'translate-pair bindings)
                           response)))
      (make-variable words)))

(defun translate-pair (pair)
  "Translate the value part of a pair into an equation or expression."
  (cons (binding-var pair)
        (translate-to-expression (binding-val pair))))

(defun create-list-of-equations (exp)
  "Separate out equations embedded in nested parens."
  (cond ((null exp) nil)
        ((atom (first exp)) (list exp))
        (t (append (create-list-of-equations (first exp))
                   (create-list-of-equations (rest exp))))))

(defun make-variable (words)
  "Create a variable name based on the given list of words."
  ;;Noise will already have been removed
  (first words))

(defun noise-word-p (word)
  "Is this a low-content word that can safely be ignored?"
  (member word '(a an the this number of $)))

(defun solve-equations (equations)
  "Print the equations and their solutions."
  (print-equations "The equations to be solved are: " equations)
  (print-equations "The solution is " (solve equations)))

(defun solve (equations &optional (known nil))
  "Solve a system of equations by constraint propogation"
  ;;Try to solve for one equation and subsititue its value into the others.
  ;;If that doesn't work, return to what is known
  (or (some #'(lambda (equation)
                (let ((x (one-unknown equation)))
                  (when x
                    (let ((answer (solve-arithmetic
                                   (isolate equation x))))
                      (solve (subst (exp-rhs answer) (exp-lhs answer)
                                    (remove equation equations))
                             (cons answer known))))))
            equations)
      known))

(defun isolate (e x)
  "Isolate the lone x in e on the left-hand side of e."
  ;;This assumes there is exactly one x in e and that e is an equation.
  (cond ((eq (exp-lhs e) x)
         ;;Case I: X = A -> x = n
         e)
        ((in-exp x (exp-rhs e))
         ;;Case II: A = f(x) -> f(x) = A
         (isolate (mkexp (exp-rhs e)
                         '= (exp-lhs e)) x))
        ((in-exp x (exp-lhs (exp-lhs e)))
         ;;Case III: f(x) * A = B -> f(x) = B / A
         (isolate (mkexp (exp-lhs (exp-lhs e))
                         '= (mkexp (exp-rhs e)
                                   (inverse-op (exp-op (exp-lhs e)))
                                   (exp-rhs (exp-lhs e)))) x))
        ((commutative-p (exp-op (exp-lhs e)))
         ;;Case IV: A * f(x) = B -> f(x) = B / A
         (isolate (mkexp (exp-rhs (exp-lhs e))
                         '= (mkexp (exp-rhs e)
                                   (inverse-op (exp-op (exp-lhs e)))
                                   (exp-lhs (exp-lhs e)))) x))
        (t ;;Case V: A / f(x) = B -> f(x) = A / B
         (isolate (mkexp (exp-rhs (exp-lhs e))
                         '= (mkexp (exp-lhs (exp-lhs e))
                                   (exp-op (exp-lhs e))
                                   (exp-rhs e))) x))))

(defun print-equations (header equations)
  "Print a list of equations."
  (format t "~%~a~{~%  ~{ ~a~}~}~%" header
          (mapcar #'prefix->infix equations)))

(defun inverse-op (op)
  "Return the inverse operator of op."
  (second (assoc op operators-and-inverses)))

(defun unknown-p (exp)
  "Return true if exp a known symbol."
  (symbolp exp))

(defun in-exp (x exp)
  "Return true if x appears anywhere in exp."
  (or (eq x exp)
      (and (listp exp)
           (or (in-exp x (exp-lhs exp))
               (in-exp x (exp-rhs exp))))))

(defun no-unknown (exp)
  "Return true if there are no unknown in exp."
  (cond ((unknown-p exp) nil)
        ((atom exp) t)
        ((no-unknown (exp-lhs exp)) (no-unknown (exp-rhs exp)))
        (t nil)))

(defun one-unknown (exp)
  "Return the single unknown in exp if there is only one."
  (cond ((unknown-p exp) exp)
        ((no-unknown (exp-lhs exp)) (one-unknown (exp-rhs exp)))
        ((no-unknown (exp-rhs exp)) (one-unknown (exp-lhs exp)))
        (t nil)))

(defun solve-arithmetic (equation)
  "Do the arithmetic for the right-hand side."
  ;This assumes the right hand side is in the correct form.
  (mkexp (exp-lhs equation) '= (eval (exp-rhs equation))))

(defun commutative-p (op)
  "Return true if the operator is commutative."
  (member op '(+ * =)))

(defun prefix->infix (exp)
  "Translate prefix notation to infix."
  (if (atom exp) exp
      (mapcar #'prefix->infix
              (if (binary-exp-p exp)
                  (list (exp-lhs exp) (exp-op exp) (exp-rhs exp))
                  exp))))

(defun binary-exp-p (exp)
  "Return true if both an exp and there is only one side, eg LHS OP or OP RHS."
  (and (exp-p exp) (= (length (exp-args exp)) 2)))










