(in-package :paip)

;;paip examples and exercises chapter 8
;;Michael Reis <michael@mtreis86.com


(defvar *simplification-rules*
      (mapcar #'simp-rule
              '((x + 0  = x)
                (0 + x  = x)
                (x + x  = 2 * x)
                (x - 0  = x)
                (0 - x  = - x)
                (x - x  = 0)
                (- - x  = x)
                (x * 1  = x)
                (1 * x  = x)
                (x * 0  = 0)
                (0 * x  = 0)
                (x * x  = x ^ 2)
                (x / 0  = undefined)
                (0 / x  = 0)
                (x / 1  = x)
                (x / x  = 1)
                (0 ^ 0  = undefined)
                (x ^ 0  = 1)
                (0 ^ x  = 0)
                (1 ^ x  = 1)
                (x ^ 1  = x)
                (x ^ -1 = 1 / x)
                (x * (y / x) = y)
                ((y / x) * x = y)
                ((y * x) / x = y)
                ((x * y) / x = y)
                (x + - x = 0)
                ((- x) + x = 0)
                (x + y - x = y))))

(defun simp-rule (rule)
  "Transform a rule into the proper format."
  (let ((exp (infix->prefix rule)))
    (mkexp (expand-pat-match-abbrev (exp-lhs exp))
           (exp-op exp) (exp-rhs exp))))
