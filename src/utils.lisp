(in-package :paip)

;;paip examples and exercises utilities
;;Michael Reis <michael@mtreis86.com

(setf (symbol-function 'find-all-if) #'remove-if-not)

(defun mappend (fn &rest lists)
  "Apply fn to each element of the list and append the results"
  (apply #'append (apply #'mapcar fn lists)))

(defun flatten (input &optional accumulator)
  "Return a flat list of the atoms in the input, EG ((a (b c)) d) -> (a b c d)."
  (cond ((null input) accumulator)
        ((atom input) (cons input accumulator))
        (t (flatten (first input)
                    (flatten (rest input)
                             accumulator)))))

(proclaim '(inline reuse-cons))

(defun reuse-cons (x y x-y)
  "Return (cons x y) or just x-y if it is equal to (cons x y)."
  (if (and (eql x (first x-y)) (eql y (rest x-y)))
      x-y
      (cons x y)))

(defun find-all (item sequence &rest keyword-args &key (test #'eql) test-not
                 &allow-other-keys)
  "Find all those elements of sequence that match item, according to the keywords. Doesn't alter sequence"
  (if test-not
      (apply #'remove item sequence
             :test-not (complement test-not) keyword-args)
      (apply #'remove item sequence
             :test (complement test) keyword-args)))

(defun length=1 (x)
  "Is x a list of length 1?"
  (and (consp x) (null (rest x))))

(defun random-elt (choices)
  "Pick an element from a list at random."
  (elt choices (random (length choices))))

(defun one-of (set)
  "Pick one element of a set, and make a list of it."
  (list (random-elt set)))

(defstruct delay (value nil) (function nil))

(defmacro delay (&rest body)
  "A computation that can be executed later by FORCE."
  `(make-delay :function #'(lambda () . ,body)))

(defun force (x)
  "Find the value of x, by computing if it is a delay."
  (if (not (delay-p x))
      x
      (progn
        (when (delay-function x)
          (setf (delay-value x) (funcall (delay-function x)))
          (setf (delay-function x) nil))
        (delay-value x))))

(defun rest2 (x)
  "The rest of a list after the first TWO elements."
  (rest (rest x)))




















