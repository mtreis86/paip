

;;N-lists
;;(count . element) pair
(defun make-empty-nlist ()
  "Make a new, empty nlist."
  (cons 0 nil))

(defun nlist-n (x)
  "The number of elements in an nlist."
  (car x))

(defun nlist-list (x)
  "The elements in an nlist"
  (cdr x))

(defun nlist-push (item nlist)
  "Add a new element to an nlist."
  (incf (car nlist))
  (push item (cdr nlist))
  nlist)

;;Discrimination trees
(defstruct (dtree (:type vector))
          (first nil) (rest nil) (atoms nil) (var (make-empty-nlist)))

(defvar *predicates* nil)

(defun get-dtree (predicate)
  "Fetch or make the dtree for this predicate."
  (cond ((get predicate 'dtree))
        (t (push predicate *predicates*)
           (setf (get predicate 'dtree) (make-dtree)))))

(defun clear-dtrees ()
  "Remove all the dtrees for all the predicates."
  (dolist (predicate *predicates*)
    (setf (get predicate 'dtree) nil))
  (setf *predicates* nil))

(defun index (key)
  "Store key in a dtree node. Key must be (predicate . args). It is stored in the predicate's dtree."
  (dtree-index key key (get-dtree (predicate key))))

(defun dtree-index (key value dtree)
  "Index value under all the atoms of key in the dtree."
  (cond
    ((consp key) ; Index on both first and rest
     (dtree-index (first key) value
                  (or (dtree-first dtree)
                      (setf (dtree-first dtree) (make-dtree))))
     (dtree-index (rest key) value
                  (or (dtree-rest dtree)
                      (setf (dtree-rest dtree) (make-dtree)))))
    ((null key)) ; Don't index on nil
    ((variable-p key) ; Index a variable
     (nlist-push value (dtree-var dtree)))
    (t ; Make sure there is an nlist for this atom, and add to it
     (nlist-push value (lookup-atom key dtree)))))

(defun lookup-atom (atom dtree)
  "Return or create the nlist for this atom in dtree."
  (or (lookup atom (dtree-atoms dtree))
      (let ((new (make-empty-nlist)))
        (push (cons atom new) (dtree-atoms dtree))
        new)))

(defun lookup (key alist)
  "Get the cdr of the key's entry in the association list."
  (cdr (assoc key alist)))

;;Dtree test
(defun test-index ()
  (let ((props '((p a b) (p a c) (p a ?x) (p b c)
                 (p b (f c)) (p a (f . ?x)))))
    (clear-dtrees)
    (mapc #'index props)
    (write (list props (get-dtree 'p))
           :circle t :array t :pretty t)
    (values)))

(defun fetch (query))


(defun dtree-fetch (pat dtree var-list-in var-n-in best-list best-n)
  "Return two values, a list-of-lists of possible matches to pat, and the number of elements in that list."
  ())

























