;;paip examples and exercises chapter 3
;;Michael Reis <michael@mtreis86.com


(defun length1 (list)
  (let ((len 0))
    (dolist (element list)
      (incf len))
    len))

(defun length1.1 (list)
  (let ((len 0))
    (dolist (element list len)
      (incf len))))

(defun length2 (list)
  (let ((len 0))
    (mapc #'(lambda (element)
              (incf len))
          list)
    len))

(defun length3 (list)
  (do ((len 0 (+ len 1))
       (l list (rest l)))
      ((null l) len)))

(defun length4 (list)
  (loop for element in list
        count t))

(defun length5 (list)
  (loop for element in list
        summing 1))

(defun length6 (list)
  (loop with len = 0
        until (null list)
        for element = (pop list)
        do (incf len)
        finally (return len)))

(defun length7 (list)
  (count-if #'true list))

(defun true (x) t)

(defun length8 (list)
  (if (null list)
      0
      (+ 1 (position-if #'true list :from-end t))))

(defun length9 (list)
  (if (null list)
      0
      (+ 1 (length9 (rest list)))))

(defun length10 (list)
  (length10-aux list 0))

(defun length10-aux (sublist len-so-far)
  (if (null sublist)
      len-so-far
      (length10-aux (rest sublist) (+ 1 len-so-far))))

(defun length11 (list &optional (len-so-far 0))
  (if (null list)
      len-so-far
      (length11 (rest list) (+ 1 len-so-far))))

(defun length12 (the-list)
  (labels
    ((length13 (list len-so-far)
       (if (null list)
           len-so-far
           (length13 (rest list) (+ 1 len-so-far)))))
    (length13 the-list 0)))

;; (defmacro while (test &rest body)
;;   "Repeat body while test is true"
;;   (list* 'loop
;;          (list 'unless test '(return nil))
;;          body))

;; (defmacro while (test &rest body)
;;   "Repeat body while test is true"
;;   (let ((code '(loop (unless test (return nil)) . body)))
;;     (subst test 'test (subst body 'body code))))

;; (defmacro while (test &rest body)
;;   "Repeat body while test is true"
;;   `(loop (unless ,test (return nil))
;;          ,@body))

(defun dprint (exp)
  "Rewrite an expression as a dot product"
  (cond
    ((atom exp) (princ exp))
    (t (princ "(")
       (dprint (first exp))
       (pr-rest (rest exp))
       (princ ")")
       exp)))

(defun pr-rest (exp)
  (princ " . ")
  (dprint exp))

(defun find-all (item sequence &rest keyword-args &key (test #'eql) test-not
                 &allow-other-keys)
  "Find all those elements of sequence that match item, according to the keywords. Doesn't alter sequence"
  (if test-not
      (apply #'remove item sequence
             :test-not (complement test-not) keyword-args)
      (apply #'remove item sequence
             :test (complement test) keyword-args)))

(defun length14 (list &aux (len 0))
  (dolist (element list len)
    (incf len)))

(defun length15 (list)
  (reduce #'length15-helper list :initial-value 0))

(defun length15-helper (count list)
  (incf count))

